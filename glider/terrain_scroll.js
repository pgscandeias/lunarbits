function AGC (instruments, terrain) {
    /* Convenience */
    i = instruments;
    abs = {
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };
    rotation = 0;
    thrust = 0;

    var terrainMultiplier = Math.ceil(i.x / 900) - 1;
    function getTerrain(x) {
        var factor = terrainMultiplier;
        return Math.floor(x - 900 * factor);
    }

    function upright() {
        if (abs.o < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }

    function hover(tolerance) {
        if (i.v < tolerance) thrust = 0;
        if (i.v > tolerance) {
            thrust = 1;
            if (Math.abs(i.o) > 45) rotation = i.o > 0 ? -1 : 1;
        }
    }

    upright();
    hover(0);

    console.log(terrain[getTerrain(i.x)]);

    /* Output */
    return [thrust, rotation];
}