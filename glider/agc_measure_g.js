function AGC (instruments, terrain) {
    i = instruments;
    abs = {
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };
    rotation = 0;
    thrust = 0;

    if (!window.ship) window.ship = {};
    if (!window.ship.prevV ) window.ship.prevV = i.h;

    if (Math.abs(i.o) < 10) rotation = 0;
    else rotation = i.o > 0 ? -1 : 1;

    console.log(i.v - window.ship.prevV);

    window.ship.prevV = i.v;

    return [thrust, rotation];
}