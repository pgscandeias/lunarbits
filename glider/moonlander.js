(function() {


	// stats.js r5 - http://github.com/mrdoob/stats.js
	var Stats=function(){var j=0,u=2,r,C=0,E=new Date().getTime(),w=E,f=E,m=0,e=1000,i=0,F,q,c,d,B,k=0,G=1000,a=0,A,t,p,D,l,v=0,o=1000,s=0,h,n,z,g,b,y={fps:{bg:{r:16,g:16,b:48},fg:{r:0,g:255,b:255}},ms:{bg:{r:16,g:48,b:16},fg:{r:0,g:255,b:0}},mem:{bg:{r:48,g:16,b:26},fg:{r:255,g:0,b:128}}};r=document.createElement("div");r.style.fontFamily="Helvetica, Arial, sans-serif";r.style.textAlign="left";r.style.fontSize="9px";r.style.opacity="0.9";r.style.width="80px";r.style.cursor="pointer";r.addEventListener("click",H,false);F=document.createElement("div");F.style.backgroundColor="rgb("+Math.floor(y.fps.bg.r/2)+","+Math.floor(y.fps.bg.g/2)+","+Math.floor(y.fps.bg.b/2)+")";F.style.padding="2px 0px 3px 0px";r.appendChild(F);q=document.createElement("div");q.innerHTML="<strong>FPS</strong>";q.style.color="rgb("+y.fps.fg.r+","+y.fps.fg.g+","+y.fps.fg.b+")";q.style.margin="0px 0px 1px 3px";F.appendChild(q);c=document.createElement("canvas");c.width=74;c.height=30;c.style.display="block";c.style.marginLeft="3px";F.appendChild(c);d=c.getContext("2d");d.fillStyle="rgb("+y.fps.bg.r+","+y.fps.bg.g+","+y.fps.bg.b+")";d.fillRect(0,0,c.width,c.height);B=d.getImageData(0,0,c.width,c.height);A=document.createElement("div");A.style.backgroundColor="rgb("+Math.floor(y.ms.bg.r/2)+","+Math.floor(y.ms.bg.g/2)+","+Math.floor(y.ms.bg.b/2)+")";A.style.padding="2px 0px 3px 0px";A.style.display="none";r.appendChild(A);t=document.createElement("div");t.innerHTML="<strong>MS</strong>";t.style.color="rgb("+y.ms.fg.r+","+y.ms.fg.g+","+y.ms.fg.b+")";t.style.margin="0px 0px 1px 3px";A.appendChild(t);p=document.createElement("canvas");p.width=74;p.height=30;p.style.display="block";p.style.marginLeft="3px";A.appendChild(p);D=p.getContext("2d");D.fillStyle="rgb("+y.ms.bg.r+","+y.ms.bg.g+","+y.ms.bg.b+")";D.fillRect(0,0,p.width,p.height);l=D.getImageData(0,0,p.width,p.height);try{if(webkitPerformance&&webkitPerformance.memory.totalJSHeapSize){u=3}}catch(x){}h=document.createElement("div");h.style.backgroundColor="rgb("+Math.floor(y.mem.bg.r/2)+","+Math.floor(y.mem.bg.g/2)+","+Math.floor(y.mem.bg.b/2)+")";h.style.padding="2px 0px 3px 0px";h.style.display="none";r.appendChild(h);n=document.createElement("div");n.innerHTML="<strong>MEM</strong>";n.style.color="rgb("+y.mem.fg.r+","+y.mem.fg.g+","+y.mem.fg.b+")";n.style.margin="0px 0px 1px 3px";h.appendChild(n);z=document.createElement("canvas");z.width=74;z.height=30;z.style.display="block";z.style.marginLeft="3px";h.appendChild(z);g=z.getContext("2d");g.fillStyle="#301010";g.fillRect(0,0,z.width,z.height);b=g.getImageData(0,0,z.width,z.height);function I(N,M,K){var J,O,L;for(O=0;O<30;O++){for(J=0;J<73;J++){L=(J+O*74)*4;N[L]=N[L+4];N[L+1]=N[L+5];N[L+2]=N[L+6]}}for(O=0;O<30;O++){L=(73+O*74)*4;if(O<M){N[L]=y[K].bg.r;N[L+1]=y[K].bg.g;N[L+2]=y[K].bg.b}else{N[L]=y[K].fg.r;N[L+1]=y[K].fg.g;N[L+2]=y[K].fg.b}}}function H(){j++;j==u?j=0:j;F.style.display="none";A.style.display="none";h.style.display="none";switch(j){case 0:F.style.display="block";break;case 1:A.style.display="block";break;case 2:h.style.display="block";break}}return{domElement:r,update:function(){C++;E=new Date().getTime();k=E-w;G=Math.min(G,k);a=Math.max(a,k);I(l.data,Math.min(30,30-(k/200)*30),"ms");t.innerHTML="<strong>"+k+" MS</strong> ("+G+"-"+a+")";D.putImageData(l,0,0);w=E;if(E>f+1000){m=Math.round((C*1000)/(E-f));e=Math.min(e,m);i=Math.max(i,m);I(B.data,Math.min(30,30-(m/100)*30),"fps");q.innerHTML="<strong>"+m+" FPS</strong> ("+e+"-"+i+")";d.putImageData(B,0,0);if(u==3){v=webkitPerformance.memory.usedJSHeapSize*9.54e-7;o=Math.min(o,v);s=Math.max(s,v);I(b.data,Math.min(30,30-(v/2)),"mem");n.innerHTML="<strong>"+Math.round(v)+" MEM</strong> ("+Math.round(o)+"-"+Math.round(s)+")";g.putImageData(b,0,0)}f=E;C=0}}}};

	var Vector2 = function (x,y) {

		this.x= x || 0; 
		this.y = y || 0; 

	};



	Vector2.prototype = {

		reset: function ( x, y ) {

			this.x = x;
			this.y = y;

			return this;

		},

		toString : function (decPlaces) {
			decPlaces = decPlaces || 3; 
			var scalar = Math.pow(10,decPlaces); 
			return "[" + Math.round (this.x * scalar) / scalar + ", " + Math.round (this.y * scalar) / scalar + "]";
		},

		clone : function () {
			return new Vector2(this.x, this.y);
		},

		copyTo : function (v) {
			v.x = this.x;
			v.y = this.y;
		},

		copyFrom : function (v) {
			this.x = v.x;
			this.y = v.y;
		},	

		magnitude : function () {
			return Math.sqrt((this.x*this.x)+(this.y*this.y));
		},

		magnitudeSquared : function () {
			return (this.x*this.x)+(this.y*this.y);
		},

		normalise : function () {

			var m = this.magnitude();

			this.x = this.x/m;
			this.y = this.y/m;

			return this;	
		},

		reverse : function () {
			this.x = -this.x;
			this.y = -this.y;

			return this; 
		},

		plusEq : function (v) {
			this.x+=v.x;
			this.y+=v.y;

			return this; 
		},

		plusNew : function (v) {
			return new Vector2(this.x+v.x, this.y+v.y); 
		},

		minusEq : function (v) {
			this.x-=v.x;
			this.y-=v.y;

			return this; 
		},

		minusNew : function (v) {
			return new Vector2(this.x-v.x, this.y-v.y); 
		},	

		multiplyEq : function (scalar) {
			this.x*=scalar;
			this.y*=scalar;

			return this; 
		},

		multiplyNew : function (scalar) {
			var returnvec = this.clone();
			return returnvec.multiplyEq(scalar);
		},

		divideEq : function (scalar) {
			this.x/=scalar;
			this.y/=scalar;
			return this; 
		},

		divideNew : function (scalar) {
			var returnvec = this.clone();
			return returnvec.divideEq(scalar);
		},

		dot : function (v) {
			return (this.x * v.x) + (this.y * v.y) ;
		},

		angle : function (useRadians) {

			return Math.atan2(this.y,this.x) * (useRadians ? 1 : Vector2Const.TO_DEGREES);

		},

		rotate : function (angle, useRadians) {

			var cosRY = Math.cos(angle * (useRadians ? 1 : Vector2Const.TO_RADIANS));
			var sinRY = Math.sin(angle * (useRadians ? 1 : Vector2Const.TO_RADIANS));

			Vector2Const.temp.copyFrom(this); 

			this.x= (Vector2Const.temp.x*cosRY)-(Vector2Const.temp.y*sinRY);
			this.y= (Vector2Const.temp.x*sinRY)+(Vector2Const.temp.y*cosRY);

			return this; 
		},	

		equals : function (v) {
			return((this.x==v.x)&&(this.y==v.x));
		},

		isCloseTo : function (v, tolerance) {	
			if(this.equals(v)) return true;

			Vector2Const.temp.copyFrom(this); 
			Vector2Const.temp.minusEq(v); 

			return(Vector2Const.temp.magnitudeSquared() < tolerance*tolerance);
		},

		rotateAroundPoint : function (point, angle, useRadians) {
			Vector2Const.temp.copyFrom(this); 
			//trace("rotate around point "+t+" "+point+" " +angle);
			Vector2Const.temp.minusEq(point);
			//trace("after subtract "+t);
			Vector2Const.temp.rotate(angle, useRadians);
			//trace("after rotate "+t);
			Vector2Const.temp.plusEq(point);
			//trace("after add "+t);
			this.copyFrom(Vector2Const.temp);

		}, 

		isMagLessThan : function (distance) {
			return(this.magnitudeSquared()<distance*distance);
		},

		isMagGreaterThan : function (distance) {
			return(this.magnitudeSquared()>distance*distance);
		}


		// still AS3 to convert : 
		// public function projectOnto(v:Vector2) : Vector2
		// {
		// 		var dp:Number = dot(v);
		// 
		// 		var f:Number = dp / ( v.x*v.x + v.y*v.y );
		// 
		// 		return new Vector2( f*v.x , f*v.y);
		// 	}
		// 
		// 
		// public function convertToNormal():void
		// {
		// 	var tempx:Number = x; 
		// 	x = -y; 
		// 	y = tempx; 
		// 	
		// 	
		// }		
		// public function getNormal():Vector2
		// {
		// 	
		// 	return new Vector2(-y,x); 
		// 	
		// }
		// 
		// 
		// 
		// public function getClosestPointOnLine ( vectorposition : Point, targetpoint : Point ) : Point
		// {
		// 	var m1 : Number = y / x ;
		// 	var m2 : Number = x / -y ;
		// 	
		// 	var b1 : Number = vectorposition.y - ( m1 * vectorposition.x ) ;
		// 	var b2 : Number = targetpoint.y - ( m2 * targetpoint.x ) ;
		// 	
		// 	var cx : Number = ( b2 - b1 ) / ( m1 - m2 ) ;
		// 	var cy : Number = m1 * cx + b1 ;
		// 	
		// 	return new Point ( cx, cy ) ;
		// }
		// 

	};

	Vector2Const = {
		TO_DEGREES : 180 / Math.PI,		
		TO_RADIANS : Math.PI / 180,
		temp : new Vector2()
	};
	var audio = {};
	(function(samplerate){
		this.SampleRate = samplerate || 44100;
		var SampleRate = this.SampleRate;

		// Do not modify parameters without changing code!
		var BitsPerSample = 16;
		var NumChannels = 1;
		var BlockAlign = NumChannels * BitsPerSample >> 3;
		var ByteRate = SampleRate * BlockAlign;

		// helper functions
		var chr = String.fromCharCode; // alias for getting converting int to char 

		//////////////////////
		// Wave            ///
		//////////////////////

		var waveTag="data:audio/wav;base64,";
		// constructs a wave from sample array
		var constructWave = function(data){
			return pack( ["RIFF",36+(l=data.length),"WAVEfmt ",16,1,NumChannels,SampleRate,
									ByteRate,BlockAlign,BitsPerSample,"data",l,data],"s4s4224422s4s");
		};

		// creates an audio object from sample data
		this.make = function(arr){
			return new Audio(waveTag + btoa(constructWave(arrayToData(arr))))
		};

		// creates a wave file for downloading
		this.makeWaveFile = function(arr){
			dataToFile(waveTag + btoa(constructWave(arrayToData(arr))))
		};

		//////////////////////
		// General stuff   ///
		//////////////////////

		// Converts an integer to String representation
		//   a - number
		//   i - number of bytes
		var istr = function(a,i){
			var m8 = 0xff; // 8 bit mask
			return i?chr(a&m8)+istr(a>>8,i-1):"";
		};

		// Packs array of data to a string
		//   data   - array
		//   format - s is for string, numbers denote bytes to store in
		var pack = function(data,format){
			var out="";
			for(i=0;i<data.length;i++)
			out+=format[i]=="s"?data[i]:istr(data[i],format.charCodeAt(i)-48);
			return out;
		}

		var dataToFile = function(data){
			document.location.href = data;
		}

		//////////////////////
		// Audio Processing ///
		//////////////////////

		// Utilities
		//////////////////////

		// often used variables (just for convenience)
		var count,out,i,sfreq;
		var sin = Math.sin;
		var TAU = 2*Math.PI;
		var Arr = function(c){return new Array(c|0)}; // alias for creating a new array

		var clamp8bit  = function(a){return a<0?0:255<a?255:a}
		var sample8bit = function(a){return clamp((a*127+127)|0)}

		this.toTime    = function(a){return a/SampleRate}
		this.toSamples = function(a){return a*SampleRate}

		var arrayToData16bit = function(arr){
			var out="";
			var len = arr.length;
			for( i=0 ; i < len ; i++){
				var a = (arr[i] * 32767) | 0;
				a = a < -32768 ? -32768 : 32767 < a ? 32767 : a; // clamp
				a += a < 0 ? 65536 : 0;                       // 2-s complement
				out += String.fromCharCode(a & 255, a >> 8);
			};
			return out;
		}

		var arrayToData8bit = function(arr){
			var out="";
			var len = arr.length;
			for( i=0 ; i < len ; i++){
				var a = (arr[i] * 127 + 128) | 0;
				a = a < 0 ? 0 : 255 < a ? 255 : a;
				out += String.fromCharCode(a);
			};
			return out;
		}

		var arrayToData = function(arr){
			if( BitsPerSample == 16 )
				return arrayToData16bit(arr);
			else
				return arrayToData8bit(arr);
		}

		//////////////////////
		// Processing
		//////////////////////

		// adjusts volume of a buffer
		this.adjustVolume = function(data, v){
			for(i=0;i<data.length;i++)
			data[i] *= v;
			return data;
		}

		// Filters
		//////////////////////

		this.filter = function(data,func,from,to,A,B,C,D,E,F){
			from = from ? from : 1;
			to = to ? to : data.length;
			out = data.slice(0);
			for(i=from;i<to;i++)
			out[i] = func(data, out, from,to,i,A,B,C,D,E,F)
			return out;
		};
		var filter = this.filter;

		this.filters = {
			lowpass  : 
				function(data, out, from, to, pos, A){
				return out[pos-1] + A * (data[pos] - out[pos-1]);
			},
			lowpassx : 
				function(data, out, from, to, pos, A){
				return out[pos-1] + A*(to - pos)/(to-from) * (data[pos] - out[pos-1]);
			},
			highpass :
				function(data, out, from, to, pos, A){
				return A * (out[pos-1] + data[pos] - data[pos-1])
			}
		};
		var filters = this.filters;

		this.f = {
			lowpass  : function(data, from, to, A){return filter(data, filters.lowpass, from, to, A);},
			lowpassx : function(data, from, to, A){return filter(data, filters.lowpassx, from, to, A);},
			highpass : function(data, from, to, A){return filter(data, filters.highpass, from, to, A);}
		}

		// Generators
		//////////////////////

		// general sound generation
		// example:
		// generate(3, 440, Math.sin);
		this.generate = function(count, freq, func, A, B, C, D, E, F){
			var sfreq=freq*TAU/SampleRate;
			var out = Arr(count);
			for(i=0; i < count;i++)
			out[i] = func(i*sfreq,A,B,C,D,E,F);
			return out;
		}

		var lastNoise = 0;

		var generate = this.generate;
		this.generators =  {
			noise  : function(phase){
				if(phase % TAU < 4){
					lastNoise = Math.random() * 2 - 1;
				}
				return lastNoise;
			},
			uninoise : Math.random,
			sine   : Math.sin,
			synth  : function(phase){return sin(phase) + .5*sin(phase/2) + .3*sin(phase/4)},
			saw    : function(phase){return 2*(phase/TAU - ((phase/TAU + 0.5)|0))},
			square : function(phase,A){return sin(phase) > A ? 1.0 : sin(phase) < A ? -1.0 : A}
		};
		var generators = this.generators;

		this.g = {
			noise  : function(count){ return generate(count,0, generators.noise) },
			sine   : function(count, freq){ return generate(count, freq, generators.sine) },
			synth  : function(count, freq){ return generate(count, freq, generators.synth) },
			saw    : function(count, freq){ return generate(count, freq, generators.saw) },
			square : function(count, freq, A){ return generate(count, freq, generators.square, A) }
		};
	}).apply(audio);
	var jsfx = {};
	(function () {
		this.Parameters = []; // will be constructed in the end

		this.Generators = {
			square : audio.generators.square,
			saw    : audio.generators.saw,
			sine   : audio.generators.sine,
			noise  : audio.generators.noise,
			synth  : audio.generators.synth
		};

		this.getGeneratorNames = function(){
			var names = [];
			for(e in this.Generators)
				names.push(e);
			return names;
		}

		var nameToParam = function(name){
			return name.replace(/ /g, "");
		}

		this.getParameters = function () {
			var params = [];

			var grp = 0;

			// add param
			var ap = function (name, min, max, def, step) {
				if (step === undefined)
					step = (max - min) / 1000;
				var param = { name: name, id: nameToParam(name),
					min: min, max: max, step:step, def: def, 
					type: "range", group: grp};
					params.push(param);
			};

			// add option
			var ao = function(name, options, def){
				var param = {name: name, id: nameToParam(name),
					options: options, def: def,
					type: "option", group: grp };
					params.push(param);
			}

			var gens = this.getGeneratorNames();
			ao("Generator", gens, gens[0]);
			ap("Super Sampling Quality", 0, 16, 0, 1);
			ap("Master Volume",  0, 1, 0.4);
			grp++;

			ap("Attack Time",    0, 1, 0.1); // seconds
			ap("Sustain Time",   0, 2, 0.3); // seconds
			ap("Sustain Punch",  0, 3, 2);
			ap("Decay Time",     0, 2, 1); // seconds
			grp++;

			ap("Min Frequency",   20, 2400, 0, 1);
			ap("Start Frequency", 20, 2400, 440, 1);
			ap("Max Frequency",   20, 2400, 2000, 1);
			ap("Slide",           -1, 1, 0);
			ap("Delta Slide",     -1, 1, 0);

			grp++;
			ap("Vibrato Depth",     0, 1, 0);
			ap("Vibrato Frequency", 0.01, 48, 8);
			ap("Vibrato Depth Slide",   -0.3, 1, 0);
			ap("Vibrato Frequency Slide", -1, 1, 0);

			grp++;
			ap("Change Amount", -1, 1, 0);
			ap("Change Speed",  0, 1, 0.1);

			grp++;
			ap("Square Duty", 0, 0.5, 0);
			ap("Square Duty Sweep", -1, 1, 0);

			grp++;
			ap("Repeat Speed", 0, 0.8, 0);

			grp++;
			ap("Phaser Offset", -1, 1, 0);
			ap("Phaser Sweep", -1, 1, 0);

			grp++;
			ap("LP Filter Cutoff", 0, 1, 1);
			ap("LP Filter Cutoff Sweep", -1, 1, 0);
			ap("LP Filter Resonance",    0, 1, 0);
			ap("HP Filter Cutoff",       0, 1, 0);
			ap("HP Filter Cutoff Sweep", -1, 1, 0);

			return params;
		};


		/**
		 * Input params object has the same parameters as described above
		 * except all the spaces have been removed
		 *
		 * This returns an array of float values of the generated audio.
		 *
		 * To make it into a wave use:
		 *    data = jsfx.generate(params)
		 *    audio.make(data)
		 */
		this.generate = function(params){
			// useful consts/functions
			var TAU = 2 * Math.PI,
			sin = Math.sin,
			cos = Math.cos,
			pow = Math.pow,
			abs = Math.abs;
			var SampleRate = audio.SampleRate;

			// super sampling
			var super_sampling_quality = params.SuperSamplingQuality | 0;
			if(super_sampling_quality < 1) super_sampling_quality = 1;
			SampleRate = SampleRate * super_sampling_quality;

			// enveloping initialization
			var _ss = 1.0 + params.SustainPunch;
			var envelopes = [ {from: 0.0, to: 1.0, time: params.AttackTime},
				{from: _ss, to: 1.0, time: params.SustainTime},
				{from: 1.0, to: 0.0, time: params.DecayTime}];
				var envelopes_len = envelopes.length;

				// envelope sample calculation
				for(var i = 0; i < envelopes_len; i++){
					envelopes[i].samples = 1 + ((envelopes[i].time * SampleRate) | 0);
				}
				// envelope loop variables
				var envelope = undefined;
				var envelope_cur = 0.0;
				var envelope_idx = -1;
				var envelope_increment = 0.0;
				var envelope_last = -1;

				// count total samples
				var totalSamples = 0;
				for(var i = 0; i < envelopes_len; i++){
					totalSamples += envelopes[i].samples;
				}

				// fix totalSample limit
				if( totalSamples < SampleRate / 2){
					totalSamples = SampleRate / 2;
				}
				//totalSamples >>=1; 
				var outSamples = (totalSamples / super_sampling_quality)|0;

				// out data samples
				var out = new Array(outSamples);
				var sample = 0;
				var sample_accumulator = 0;

				// main generator        
				var generator = jsfx.Generators[params.Generator];
				if (generator === undefined)
					generator = this.Generators.square;
				var generator_A = 0;
				var generator_B = 0;

				// square generator
				generator_A = params.SquareDuty;
				square_slide = params.SquareDutySweep / SampleRate;

				// phase calculation
				var phase = 0;
				var phase_speed = params.StartFrequency * TAU / SampleRate;

				// phase slide calculation        
				var phase_slide = 1.0 + pow(params.Slide, 3.0) * 64.0 / SampleRate;
				var phase_delta_slide = pow(params.DeltaSlide, 3.0) / (SampleRate * 1000); 
				if (super_sampling_quality !== undefined)
					phase_delta_slide /= super_sampling_quality; // correction

				// frequency limiter
				if(params.MinFrequency > params.StartFrequency)
					params.MinFrequency = params.StartFrequency;

				if(params.MaxFrequency < params.StartFrequency)
					params.MaxFrequency = params.StartFrequency;

				var phase_min_speed = params.MinFrequency * TAU / SampleRate;
				var phase_max_speed = params.MaxFrequency * TAU / SampleRate;

				// frequency vibrato
				var vibrato_phase = 0;
				var vibrato_phase_speed = params.VibratoFrequency * TAU / SampleRate;
				var vibrato_amplitude = params.VibratoDepth;

				// frequency vibrato slide
				var vibrato_phase_slide = 1.0 + pow(params.VibratoFrequencySlide, 3.0) * 3.0 / SampleRate;
				var vibrato_amplitude_slide = params.VibratoDepthSlide / SampleRate;

				// arpeggiator
				var arpeggiator_time = 0;
				var arpeggiator_limit = params.ChangeSpeed * SampleRate;
				var arpeggiator_mod   = pow(params.ChangeAmount, 2);
				if (params.ChangeAmount > 0)
					arpeggiator_mod = 1 + arpeggiator_mod * 10;
				else
					arpeggiator_mod = 1 - arpeggiator_mod * 0.9;

				// phaser
				var phaser_max = 1024;
				var phaser_mask = 1023;
				var phaser_buffer = new Array(phaser_max);
				for(var _i = 0; _i < phaser_max; _i++)
				phaser_buffer[_i] = 0;
				var phaser_pos = 0;
				var phaser_offset = pow(params.PhaserOffset, 2.0) * (phaser_max - 4);
				var phaser_offset_slide = pow(params.PhaserSweep, 3.0) * 4000 / SampleRate;
				var phaser_enabled = (abs(phaser_offset_slide) > 0.00001) ||
					(abs(phaser_offset) > 0.00001);

				// lowpass filter
				var filters_enabled = (params.HPFilterCutoff > 0.001) || (params.LPFilterCutoff < 0.999);

				var lowpass_pos = 0;
				var lowpass_pos_slide = 0;
				var lowpass_cutoff = pow(params.LPFilterCutoff, 3.0) / 10;
				var lowpass_cutoff_slide = 1.0 + params.HPFilterCutoffSweep / 10000;
				var lowpass_damping = 5.0 / (1.0 + pow(params.LPFilterResonance, 2) * 20 ) *
					(0.01 + params.LPFilterCutoff);
				if ( lowpass_damping > 0.8)
					lowpass_damping = 0.8;
				lowpass_damping = 1.0 - lowpass_damping;
				var lowpass_enabled = params.LPFilterCutoff < 0.999;

				// highpass filter
				var highpass_accumulator = 0;
				var highpass_cutoff = pow(params.HPFilterCutoff, 2.0) / 10;
				var highpass_cutoff_slide = 1.0 + params.HPFilterCutoffSweep / 10000;

				// repeat
				var repeat_time  = 0;
				var repeat_limit = totalSamples;
				if (params.RepeatSpeed > 0){
					repeat_limit = pow(1 - params.RepeatSpeed, 2.0) * SampleRate + 32;
				}

				// master volume controller
				var master_volume = params.MasterVolume;

				var k = 0;
				for(var i = 0; i < totalSamples; i++){
					// main generator
					sample = generator(phase, generator_A, generator_B);

					// square generator
					generator_A += square_slide;
					if(generator_A < 0.0){
						generator_A = 0.0;
					} else if (generator_A > 0.5){
						generator_A = 0.5;
					}

					if( repeat_time > repeat_limit ){
						// phase reset
						phase = 0;
						phase_speed = params.StartFrequency * TAU / SampleRate;
						// phase slide reset
						phase_slide = 1.0 + pow(params.Slide, 3.0) * 3.0 / SampleRate;
						phase_delta_slide = pow(params.DeltaSlide, 3.0) / (SampleRate * 1000);
						if (super_sampling_quality !== undefined)
							phase_delta_slide /= super_sampling_quality; // correction
						// arpeggiator reset
						arpeggiator_time = 0;
						arpeggiator_limit = params.ChangeSpeed * SampleRate;
						arpeggiator_mod   = 1 + (params.ChangeAmount | 0) / 12.0;                
						// repeat reset
						repeat_time = 0;
					}
					repeat_time += 1;

					// phase calculation
					phase += phase_speed;

					// phase slide calculation
					phase_slide += phase_delta_slide;
					phase_speed *= phase_slide;

					// arpeggiator
					if ( arpeggiator_time > arpeggiator_limit ){
						phase_speed *= arpeggiator_mod;
						arpeggiator_limit = totalSamples;
					}
					arpeggiator_time += 1;

					// frequency limiter
					if (phase_speed > phase_max_speed){
						phase_speed = phase_max_speed;
					} else if(phase_speed < phase_min_speed){
						phase_speed = phase_min_speed;
					}

					// frequency vibrato
					vibrato_phase += vibrato_phase_speed;
					var _vibrato_phase_mod = phase_speed * sin(vibrato_phase) * vibrato_amplitude;
					phase += _vibrato_phase_mod;

					// frequency vibrato slide
					vibrato_phase_speed *= vibrato_phase_slide;
					if(vibrato_amplitude_slide){
						vibrato_amplitude += vibrato_amplitude_slide;
						if(vibrato_amplitude < 0){
							vibrato_amplitude = 0;
							vibrato_amplitude_slide = 0;
						} else if (vibrato_amplitude > 1){
							vibrato_amplitude = 1;
							vibrato_amplitude_slide = 0;
						}
					}

					// filters
					if( filters_enabled ){

						if( abs(highpass_cutoff) > 0.001){
							highpass_cutoff *= highpass_cutoff_slide;
							if(highpass_cutoff < 0.00001){
								highpass_cutoff = 0.00001;
							} else if(highpass_cutoff > 0.1){
								highpass_cutoff = 0.1;
							}
						}

						var _lowpass_pos_old = lowpass_pos;
						lowpass_cutoff *= lowpass_cutoff_slide;
						if(lowpass_cutoff < 0.0){
							lowpass_cutoff = 0.0;
						} else if ( lowpass_cutoff > 0.1 ){
							lowpass_cutoff = 0.1;
						}
						if(lowpass_enabled){
							lowpass_pos_slide += (sample - lowpass_pos) * lowpass_cutoff;
							lowpass_pos_slide *= lowpass_damping;
						} else {
							lowpass_pos = sample;
							lowpass_pos_slide = 0;
						}
						lowpass_pos += lowpass_pos_slide;

						highpass_accumulator += lowpass_pos - _lowpass_pos_old;
						highpass_accumulator *= 1.0 - highpass_cutoff;
						sample = highpass_accumulator;  
					}

					// phaser
					if (phaser_enabled) {
						phaser_offset += phaser_offset_slide;
						if( phaser_offset < 0){
							phaser_offset = -phaser_offset;
							phaser_offset_slide = -phaser_offset_slide;
						}
						if( phaser_offset > phaser_mask){
							phaser_offset = phaser_mask;
							phaser_offset_slide = 0;
						}

						phaser_buffer[phaser_pos] = sample;
						// phaser sample modification
						var _p = (phaser_pos - (phaser_offset|0) + phaser_max) & phaser_mask;
						sample += phaser_buffer[_p];
						phaser_pos = (phaser_pos + 1) & phaser_mask;
					}

					// envelope processing
					if( i > envelope_last ){
						envelope_idx += 1;
						if(envelope_idx < envelopes_len) // fault protection
							envelope = envelopes[envelope_idx];
						else // the trailing envelope is silence
							envelope = {from: 0, to: 0, samples: totalSamples};
						envelope_cur = envelope.from;
						envelope_increment = (envelope.to - envelope.from) / (envelope.samples + 1);
						envelope_last += envelope.samples;
					}
					sample *= envelope_cur;
					envelope_cur += envelope_increment;

					// master volume controller
					sample *= master_volume;

					// prepare for next sample
					if(super_sampling_quality > 1){
						sample_accumulator += sample;
						if( (i + 1) % super_sampling_quality === 0){
							out[k] = sample_accumulator / super_sampling_quality;
							k += 1;
							sample_accumulator = 0;
						}
					} else {
						out[i] = sample;
					}
				}

				// return out;

				// add padding 10ms
				var len = (SampleRate / 100)|0;
				padding = new Array(len);
				for(var i = 0; i < len; i++)
				padding[i] = 0;
				return padding.concat(out).concat(padding);
				//return out.slice(10000,-10000);
		}

		this.Parameters = this.getParameters();

	}).apply(jsfx);
	var jsfxlib = {};
	(function () {
		// takes object with param arrays
		// audiolib = {
		//   Sound : ["sine", 1, 2, 4, 1...
		// }
		//
		// returns object with audio samples
		// p.Sound.play()
		this.createWaves = function(lib){
			var sounds = {};
			for (var e in lib) {
				var data = lib[e];
				sounds[e] = this.createWave(data);
			}
			return sounds;
		}

		/* Create a single sound:
			 var p = jsfxlib.createWave(["sine", 1,2,3, etc.]);
			 p.play();
			 */
		this.createWave = function(lib) {
			var params = this.arrayToParams(lib),
			data = jsfx.generate(params),
			wave = audio.make(data);

			return wave;
		}

		this.paramsToArray = function(params){
			var pararr = [];
			var len = jsfx.Parameters.length;
			for(var i = 0; i < len; i++){
				pararr.push(params[jsfx.Parameters[i].id]);
			}
			return pararr;
		}

		this.arrayToParams = function(pararr){
			var params = {};
			var len = jsfx.Parameters.length;
			for(var i = 0; i < len; i++){
				params[jsfx.Parameters[i].id] = pararr[i];
			}
			return params;
		}
	}).apply(jsfxlib);
	// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
	// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

	// requestAnimationFrame polyfill by Erik Möller
	// fixes from Paul Irish and Tino Zijdel

	(function() {
		var lastTime = 0;
		var vendors = ['ms', 'moz', 'webkit', 'o'];
		for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
			window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
			window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
			|| window[vendors[x]+'CancelRequestAnimationFrame'];
		}

		if (!window.requestAnimationFrame)
			window.requestAnimationFrame = function(callback, element) {
				var currTime = new Date().getTime();
				var timeToCall = Math.max(0, 16 - (currTime - lastTime));
				var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
																	 timeToCall);
																	 lastTime = currTime + timeToCall;
																	 return id;
			};

			if (!window.cancelAnimationFrame)
				window.cancelAnimationFrame = function(id) {
					clearTimeout(id);
				};
	}());
	
	function InfoDisplay(width, height) { 

		var dom = this.domElement = document.createElement('div'); 
		var s = dom.style; 
		s.position = 'absolute';
		s.top = '0px';
		//	s.color = 'white';
		//	s.fontFamily = 'Courier';
		//	s.textTransform = 'uppercase';

		var w = width,
		h = height, 
		LEFT = 0, 
		CENTRE = 1, 
		RIGHT = 2; 


		var scoreLabel = makeInfoBox("Score"),
		timeLabel = makeInfoBox("Time"),
		fuelLabel = makeInfoBox("Fuel"), 
		score = this.score = makeInfoBox("0000"), 
		time = this.time = makeInfoBox("0:00"), 
		fuel = this.fuel = makeInfoBox("0000"),

		altLabel = makeInfoBox("Altitude", LEFT, 250),
		horizSpeedLabel = makeInfoBox("Horizontal Speed", LEFT, 250),
		vertSpeedLabel = makeInfoBox("Vertical Speed", LEFT, 250),

		alt = this.alt = makeInfoBox("000", RIGHT),
		horizSpeed = this.horizSpeed = makeInfoBox("000", RIGHT),
		vertSpeed = this.vertSpeed = makeInfoBox("000", RIGHT), 

		thrustLabel = makeInfoBox("thrust", LEFT, 250),
		rotateLabel = makeInfoBox("rotation", LEFT, 250),
		thrust = this.thrust = makeInfoBox("0", RIGHT),
		rotate = this.rotate = makeInfoBox("0", RIGHT),

		messages = this.messages = makeInfoBox("TEST", CENTRE, 300),
		
		updateFW = this.updateFW = makeInfoBox("UPDATE FIRMWARE", CENTRE, 300);


		this.arrangeBoxes = arrangeBoxes; 


		arrangeBoxes(w,h); 

		function makeInfoBox(text, align, width) { 

			var infobox = new InfoBox(align, width); 
			dom.appendChild(infobox.domElement); 
			infobox.setText(text); 


			return infobox;
		}

		function arrangeBoxes(width, height) { 
			w = width; 
			h = height; 

			var leftMargin = Math.min(50, width/12), 
			topMargin = Math.min(50, height/12), 
			rightMargin = w-Math.min(50, width/10), 
			vSpace = 20, 
			column2Left = leftMargin+((SCREEN_WIDTH<650)?46:80), 
			column3Left = rightMargin- ((SCREEN_WIDTH<650)?160:280);  

			//console.log(column3Left); 


			scoreLabel.setX(leftMargin);
			timeLabel.setX(leftMargin);
			fuelLabel.setX(leftMargin);

			score.setX(column2Left);
			time.setX(column2Left);
			fuel.setX(column2Left);

			altLabel.setX(column3Left); 
			horizSpeedLabel.setX(column3Left); 
			vertSpeedLabel.setX(column3Left); 
	
			thrustLabel.setX(column3Left);
			rotateLabel.setX(column3Left);

			alt.setX(rightMargin); 
			horizSpeed.setX(rightMargin); 
			vertSpeed.setX(rightMargin); 

			thrust.setX(rightMargin);
			rotate.setX(rightMargin);

			var ypos = topMargin; 


			scoreLabel.setY(ypos); 
			score.setY(ypos); 
			altLabel.setY(ypos); 
			alt.setY(ypos); 


			ypos+=vSpace; 

			timeLabel.setY(ypos); 
			time.setY(ypos); 
			horizSpeedLabel.setY(ypos); 
			horizSpeed.setY(ypos); 

			ypos+=vSpace; 

			fuelLabel.setY(ypos); 
			fuel.setY(ypos); 

			vertSpeedLabel.setY(ypos); 
			vertSpeed.setY(ypos);

			ypos+=vSpace; 
			thrustLabel.setY(ypos);
			thrust.setY(ypos);
			ypos+=vSpace; 
			rotateLabel.setY(ypos);
			rotate.setY(ypos);
			
			messages.setX(width/2); 
			messages.setY(((height-topMargin)/4)+topMargin); 

			updateFW.setX(width/2);
			updateFW.setY(height-topMargin);

			document.body.style.fontSize = '50%'; 


		}


		this.showGameInfo = function(msg) { 

			messages.show(); 
			return messages.setText(msg); // returns true if message was changed.

		};
		this.hideGameInfo = function () { 
			messages.setText(''); 
			messages.hide(); 

		};

		this.updateBoxInt = function(boxname, value, padding) { 

			value = Math.floor(value); 

			if(padding>0) { 
				value = ''+value;
				while((value.length===undefined) || (value.length<padding)) {
					value = '0'+value; 
				}

			}

			if(this[boxname]) {
				this[boxname].setText(value); 
			}

		};

		this.updateBoxTime = function(boxname, value) { 

			value = Math.floor(value); 

			secs = Math.floor(value/1000); 
			mins = Math.floor(secs/60);
			secs = secs%60; 
			if(secs<10) secs = "0"+secs; 
			//  
			// 	
			// if(padding>0) { 
			// 	value = ''+value;
			// 	while((value.length===undefined) || (value.length<padding)) {
			// 		value = '0'+value; 
			// 	}
			// 	
			// }
			// 	
			this[boxname].setText(mins+":"+secs); 


		};
	}

	function InfoBox(align, width) { 

		width = (typeof width !== 'undefined') ? width : 200; 

		var dom = this.domElement =  document.createElement('div'), 
		content = '';

		this.hidden = false; 

		dom.style.position = 'absolute';
		//	dom.style.border = '1px red solid';
		dom.style.width = width+'px';
		dom.style.display = 'block';
		dom.className = 'infoBox';

		//console.log('width', width); 

		if(align == 2) dom.style.textAlign = 'right';
		else if(align == 1) dom.style.textAlign = 'center';

		this.setText = function(text) { 
			if(text!=content) {
				content = text; 
				dom.innerHTML = text; 
				return true; 
			} else  { 
				return false; 
			}

		};
		this.setX = function(x) { 
			if(align == 2) x-=width; 
			else if(align ==1) x-=width/2; 

			dom.style.left = x+'px';
		};
		this.setY = function(y) { 
			dom.style.top = y+'px';		
		};

		this.hide = function() { 
			if(!this.hidden) 
				dom.style.display = 'none'; 
			this.hidden = true; 

		};

		this.show = function() { 
			if(this.hidden) 
				dom.style.display = 'block'; 
			this.hidden = false; 

		};

	}

	Lander = function() { 

		var vel = this.vel = new Vector2(0, 0),
		pos = this.pos = new Vector2(0,0),
		bottomLeft = this.bottomLeft = new Vector2(0,0),
		bottomRight = this.bottomRight = new Vector2(0,0), 
		thrustVec = new Vector2(0,0),
		gravity = 0.0005,
		thrust = 0.0015,
		thrustBuild = 0,
		topSpeed = 0.35, 
		drag = 0.9997, 
		bouncing = 0, 
		exploding = false, 
		targetRotation = 0,

		lastRotationTime = 0, 
		counter = 0;  

		this.rotation = 0; 
		this.thrusting = false;
		this.altitude = 0;
		this.active = true; 
		this.fuel = 0; 
		this.scale = 0.8; 
		this.left = 0; 
		this.right = 0; 
		this.bottom = 0; 
		this.top = 0; 
		this.colour = 'white';
		var shapes = this.shapes = [],
		shapePos = this.shapePos = [],
		shapeVels = this.shapeVels = []; 


		var reset = this.reset = function () { 

			vel.reset(0.4, 0); 
			pos.reset(110,150); 
			this.rotation = targetRotation = -90; 
			scale = 1; 
			thrustBuild = 0; 
			bouncing = 0; 
			this.active = true; 
			exploding = false; 
			for(var i=0; i<shapePos.length; i++) { 
				shapePos[i].reset(0,0); 
			}

		};

		reset(); 



		this.rotate = function(direction) { 
			var now = new Date().getTime();
			if(now - lastRotationTime > 80) {
				this.doRotation = direction;

				targetRotation+=direction*15; 
				targetRotation = clamp(targetRotation, -90, 90); 

				lastRotationTime = now; 
			}

		};
		this.thrust = function (state) { 
			this.thrusting = state; 
		};

		this.update = function() { 

			counter++; 

			this.rotation += (	targetRotation-this.rotation)*0.3;
			if(Math.abs(this.rotation-targetRotation)<0.1) this.rotation = targetRotation; 

			if(exploding) this.updateShapesAnimation(); 

			if(this.active) { 

				if(this.thrusting && (this.fuel>0)) { // also check for fuel 
					thrustBuild+=0.1; 
				} else { 
					thrustBuild-=0.1; 
				}
				thrustBuild = clamp(thrustBuild, 0, 1); 

				if(thrustBuild>0) { 
					thrustVec.reset(0,-thrust); 
					thrustVec.rotate(this.rotation); 
					vel.plusEq(thrustVec); 
					this.fuel -= (0.2 * thrustBuild);
				}

				pos.plusEq(vel); 
				vel.x*=drag; 
				vel.y+=gravity; 
				if(vel.y>topSpeed) velY=topSpeed; 
				else if (vel.y<-topSpeed) velY=-topSpeed; 

				this.left = pos.x-(10*this.scale); 
				this.right = pos.x+(10*this.scale); 
				this.bottom = pos.y+(14*this.scale); 
				this.top = pos.y-(5*this.scale); 
				bottomLeft.reset(this.left, this.bottom); 
				bottomRight.reset(this.right, this.bottom); 

			} else if (bouncing>0) {

				pos.y  += Math.sin(bouncing)*0.07; 
				bouncing -= Math.PI / 20;

			}
			if(this.fuel<0) this.fuel = 0; 
			setThrustVolume(thrustBuild); 

		};

		this.render = function(c, scale) { 
			c.save(); 

			c.translate(pos.x, pos.y); 
			c.scale(this.scale, this.scale); 
			c.lineWidth = 1/(this.scale * scale); 
			c.rotate(this.rotation * TO_RADIANS); 
			c.strokeStyle = this.colour; 

			c.beginPath(); 

			this.renderShapes(c);

			if((thrustBuild>0) && this.active) {
				c.lineTo(0,11+(thrustBuild*20*((((counter>>1)%3)*0.2)+1)));
				c.closePath(); 
			}	

			c.stroke(); 

			c.restore(); 
			this.colour = 'white'; 

		};
		this.crash = function () { 
			this.rotation = targetRotation = 0; 
			this.active = false; 
			exploding = true; 
			thrustBuild = 0; 

		};
		this.land = function () { 
			this.active  =false; 
			thrustBuild = 0; 
		};
		this.makeBounce = function() { 
			bouncing = Math.PI*2;	

		};

		this.defineShape = function(){

			var m='m',
			l='l',
			r='r', 
			cp='cp'; 


			var min = 2.6, max = 5; 
			var shape = []; 

			shape.push(m, min, -max); 
			shape.push(l, max, -min); 
			shape.push(l, max, min); 
			shape.push(l, min, max); 
			shape.push(l, -min, max); 
			shape.push(l, -max, min); 
			shape.push(l, -max, -min); 
			shape.push(l, -min, -max);
			shape.push(cp); 

			shapes.push(shape); 
			shapeVels.push(new Vector2(1,-2.5)); 

			shapes.push([r, -6,5,12,2]); 
			shapeVels.push(new Vector2(2,-1.5)); 

			shape = []; 
			// left leg
			shape.push(m, -5, 7.5); 
			shape.push(l, -9, 13); 
			shape.push(m, -11, 13); 
			shape.push(l, -7, 13); 
			shapes.push(shape); 
			shapeVels.push(new Vector2(0,-3)); 

			shape = []; 
			// right leg
			shape.push(m,5,7.5); 
			shape.push(l,9,13); 
			shape.push(m,11,13); 
			shape.push(l,7,13);	
			shapes.push(shape); 
			shapeVels.push(new Vector2(3,-1)); 

			shape = [];
			//thruster left
			shape.push(m,-3,7.5); 
			shape.push(l,-5,12); 
			shape.push(l,-4.5,13);
			shapes.push(shape); 
			shapeVels.push(new Vector2(1,-1)); 

			shape = [];		
			// thruster right  
			shape.push(m,3,7.5); 
			shape.push(l,5,12); 
			shape.push(l,4.5,13); 
			shapes.push(shape); 
			shapeVels.push(new Vector2(2.5,-1)); 

			shape = [];			
			// thruster bottom
			shape.push(m,4,11); 
			shape.push(l,-4,11); 
			shapes.push(shape); 
			shapeVels.push(new Vector2(2,-0.5)); 

			for(var i=0; i<shapes.length; i++) { 
				shapePos.push(new Vector2(1,-0.5)); 
			}

		};
		this.defineShape(); 

		this.updateShapesAnimation = function() { 
			if(!exploding) return; 

			for (var i=0; i<shapePos.length; i++) { 
				shapePos[i].plusEq(shapeVels[i]); 
			}

		};

		this.renderShapes = function(c) { 

			var shapes = this.shapes, 
			shapePos = this.shapePos, 
			shapeVels = this.shapeVels; 

			for (var i=0; i<shapes.length; i++) { 
				var s = shapes[i].slice(0); 

				c.save(); 
				c.translate(shapePos[i].x, shapePos[i].y); 
				//if(exploding) shapePos[i].plusEq(shapeVels[i]); 
				//console.log(i, shapePos[i], shapeVels[i]); 

				while(s.length>0) { 

					var cmd = s.shift(); 
					switch(cmd) {

						case 'm': 
							c.moveTo(s.shift(), s.shift()); 
						break; 

						case 'l': 
							c.lineTo(s.shift(), s.shift()); 
						break; 

						case 'cp': 
							c.closePath(); 
						break; 

						case 'r' : 
							c.rect(s.shift(), s.shift(), s.shift(), s.shift()); 
						break; 

						default : 
							console.log ('bad command!'); 
					}

				}
				c.restore(); 


			}




		};

	};

	function Landscape(){

		var points = this.points = [],
		lines = this.lines = [], 
		stars = this.stars = [],
		availableZones = [], 
		zoneCombis = [], 
		currentCombi = 0, 
		zoneInfos = [], 
		landscale = 1.5079165619502388,
		//landscale = 1.5, 
		rightedge;

		setupData(); 


		rightedge = this.tileWidth = points[points.length - 1].x * landscale ;

		for (var i = 0; i<points.length; i++){
			var p = points[i];
			p.x *= landscale;
			p.y *= landscale;
			p.y -= 50;

		}

		for(var i = 1;i < points.length; i++){
			var p1 = points[i-1];
			var p2 = points[i]; 
			lines.push(new LandscapeLine(p1, p2));
		}


		// make stars... 



		for(var i = 0;i < lines.length;i++)	{
			if(Math.random() < 0.1) {
				var line  = lines[i];

				var star = { x:line.p1.x, y: Math.random() *600 };

				if((star.y < line.p1.y) && (star.y < line.p2.y)) {
					stars.push(star);
				}
			}
		}	


		//	var pointcount = points.length;

		//var dirtyRectangles = []; 



		var render = this.render = function(c, view) { 

			var offset = 0; 

			while(view.left-offset>rightedge) { 
				offset+=rightedge; 
			}

			while(view.left-offset<0) { 
				offset-=rightedge;
			}

			var startOffset = offset; 

			var i = 0; 

			while(lines[i].p2.x+offset<view.left) { 
				i++; 
				if(i>lines.length) {
					i=0; 
					offset+=rightedge; 
				}
			}

			c.beginPath(); 

			var line = lines[i];
			c.moveTo(line.p1.x + offset, line.p1.y);  

			var zoneInfoIndex = 0; 

			while((line = lines[i]).p1.x+offset<view.right) { 

				var point = line.p2; 
				c.lineTo(point.x+offset, point.y);

				if((counter%20>10) && (line.multiplier!=1)){ 
					var infoBox; 

					if(!zoneInfos[zoneInfoIndex]) { 
						infoBox = zoneInfos[zoneInfoIndex] = new InfoBox(1,50); 
						document.body.appendChild(infoBox.domElement); 
					} else {
						infoBox = zoneInfos[zoneInfoIndex]; 
						infoBox.show(); 
					}
					infoBox.setText(line.multiplier+'x'); 
					infoBox.setX(((((line.p2.x-line.p1.x)/2)+line.p1.x+offset)*view.scale)+view.x); 
					infoBox.setY(((line.p2.y+2) *view.scale)+view.y); 
					zoneInfoIndex++; 

				}

				i++; 
				if(i>=lines.length) {
					i=0; 
					offset+=rightedge; 
				}


			}

			c.strokeStyle = 'white'; 
			c.lineWidth = 1/view.scale; 
			c.lineJoin = 'bevel';
			c.stroke();	

			for(var i=zoneInfoIndex; i<zoneInfos.length; i++) { 
				zoneInfos[i].hide(); 
			}


			// draw stars : 


			i = 0; 
			offset = startOffset;

			while(stars[i].x+offset<view.left) { 
				i++; 
				if(i>=stars.length) {
					i=0; 
					offset+=rightedge; 
				}
			}

			c.beginPath(); 

			while((star = stars[i]).x+offset<view.right) { 


				c.rect(star.x+offset, star.y, (1/view.scale),(1/view.scale));
				//c.lineTo(star.x+offset+(1/view.scale), star.y);

				i++; 
				if(i>=stars.length) {
					i=0; 
					offset+=rightedge; 
				}


			}		

			c.stroke(); 


			//code to check landable... 
			// c.beginPath(); 
			// 		for(var i=0; i<lines.length; i++) { 
			// 			var line = lines[i]; 
			// 			if(line.multiplier>1) { 
			// 				c.moveTo(line.p1.x, line.p1.y);	
			// 				c.lineTo(line.p2.x, line.p2.y);
			// 			}		
			// 		}	
			// 		c.strokeStyle = 'red'; 
			// 		c.stroke(); 

		};

		this.resetCombi = function () {
			currentCombi = 0;
		}

		this.setZones = function () { 
			for (var i=0; i<lines.length; i++)
			{
				lines[i].multiplier = 1;
			}	

			var combi = zoneCombis[currentCombi];

			for (var i = 0; i<combi.length; i++)
			{
				var zonenumber = combi[i];
				var zone = availableZones[zonenumber];
				line = lines[zone.lineNum];

				// var zoneLabel : TextDisplay = zoneLabels[i]; 
				// 		zoneLabel.x = line.p1.x + ((line.p2.x - line.p1.x) / 2);
				// 		zoneLabel.y = line.p1.y;
				// 		zoneLabel.text = zone.multiplier + "X";
				line.multiplier = zone.multiplier;

			}

			currentCombi++;
			if(currentCombi >= zoneCombis.length) currentCombi = 0;
		};




		function setupData() { 
			points.push(new Vector2(0.5, 355.55));
			points.push(new Vector2(5.45, 355.55));
			points.push(new Vector2(6.45, 359.4));
			points.push(new Vector2(11.15, 359.4));
			points.push(new Vector2(12.1, 363.65));
			points.push(new Vector2(14.6, 363.65));
			points.push(new Vector2(15.95, 375.75));
			points.push(new Vector2(19.25, 388));
			points.push(new Vector2(19.25, 391.9));
			points.push(new Vector2(21.65, 400));
			points.push(new Vector2(28.85, 404.25));
			points.push(new Vector2(30.7, 412.4));
			points.push(new Vector2(33.05, 416.7));
			points.push(new Vector2(37.9, 420.5));
			points.push(new Vector2(42.7, 420.5));
			points.push(new Vector2(47.4, 416.65));
			points.push(new Vector2(51.75, 409.5));
			points.push(new Vector2(56.55, 404.25));
			points.push(new Vector2(61.3, 400));
			points.push(new Vector2(63.65, 396.15));
			points.push(new Vector2(68, 391.9));
			points.push(new Vector2(70.3, 388));
			points.push(new Vector2(75.1, 386.1));
			points.push(new Vector2(79.85, 379.95));
			points.push(new Vector2(84.7, 378.95));
			points.push(new Vector2(89.05, 375.65));
			points.push(new Vector2(93.75, 375.65));
			points.push(new Vector2(98.5, 376.55));
			points.push(new Vector2(103.2, 379.95));
			points.push(new Vector2(104.3, 383.8));
			points.push(new Vector2(107.55, 388));
			points.push(new Vector2(108.95, 391.9));
			points.push(new Vector2(112.4, 396.15));
			points.push(new Vector2(113.3, 400));
			points.push(new Vector2(117.1, 404.25));
			points.push(new Vector2(121.95, 404.25));
			points.push(new Vector2(125.3, 396.3));
			points.push(new Vector2(128.6, 394.2));
			points.push(new Vector2(132.45, 396.15));
			points.push(new Vector2(135.75, 399.9));
			points.push(new Vector2(138.15, 408.15));
			points.push(new Vector2(144.7, 412.4));
			points.push(new Vector2(146.3, 424.8));
			points.push(new Vector2(149.55, 436.65));
			points.push(new Vector2(149.55, 441.05));
			points.push(new Vector2(154.35, 444.85));
			points.push(new Vector2(163.45, 444.85));
			points.push(new Vector2(168.15, 441.05));
			points.push(new Vector2(172.95, 436.75));
			points.push(new Vector2(175.45, 432.9));
			points.push(new Vector2(179.7, 428.6));
			points.push(new Vector2(181.95, 424.8));
			points.push(new Vector2(186.7, 422.5));
			points.push(new Vector2(189.15, 412.4));
			points.push(new Vector2(191.55, 404.35));
			points.push(new Vector2(196.35, 402.4));
			points.push(new Vector2(200.7, 398.1));
			points.push(new Vector2(205.45, 391.9));
			points.push(new Vector2(210.15, 383.8));
			points.push(new Vector2(212.55, 375.75));
			points.push(new Vector2(216.85, 371.8));
			points.push(new Vector2(219.3, 367.55));
			points.push(new Vector2(220.65, 363.65));
			points.push(new Vector2(224, 359.4));
			points.push(new Vector2(228.8, 359.4));
			points.push(new Vector2(233.55, 355.55));
			points.push(new Vector2(237.85, 348.45));
			points.push(new Vector2(242.65, 343.2));
			points.push(new Vector2(245, 335.15));
			points.push(new Vector2(247.35, 322.8));
			points.push(new Vector2(247.3, 314.5));
			points.push(new Vector2(248.35, 306.55));
			points.push(new Vector2(252.2, 296.5));
			points.push(new Vector2(256.55, 294.55));
			points.push(new Vector2(257.95, 290.4));
			points.push(new Vector2(261.25, 285.95));
			points.push(new Vector2(265.95, 285.95));
			points.push(new Vector2(267, 290.25));
			points.push(new Vector2(271.75, 290.25));
			points.push(new Vector2(273.25, 294.55));
			points.push(new Vector2(275.2, 294.55));
			points.push(new Vector2(278.95, 296.5));
			points.push(new Vector2(282.25, 300.3));
			points.push(new Vector2(284.7, 308.45));
			points.push(new Vector2(291.85, 312.65));
			points.push(new Vector2(298.55, 330.8));
			points.push(new Vector2(303.25, 331.8));
			points.push(new Vector2(308, 335.05));
			points.push(new Vector2(309, 338.9));
			points.push(new Vector2(312.35, 343.2));
			points.push(new Vector2(313.8, 347.05));
			points.push(new Vector2(317.05, 351.4));
			points.push(new Vector2(321.9, 351.4));
			points.push(new Vector2(322.85, 363.8));
			points.push(new Vector2(326.6, 375.75));
			points.push(new Vector2(326.6, 379.95));
			points.push(new Vector2(330.9, 379.95));
			points.push(new Vector2(332.4, 383.8));
			points.push(new Vector2(335.8, 388));
			points.push(new Vector2(338.1, 396.15));
			points.push(new Vector2(340.45, 400.1));
			points.push(new Vector2(345.3, 404.25));
			points.push(new Vector2(346.25, 416.65));
			points.push(new Vector2(349.6, 428.7));
			points.push(new Vector2(349.6, 432.85));
			points.push(new Vector2(350.95, 436.75));
			points.push(new Vector2(354.3, 441.05));
			points.push(new Vector2(359, 441.05));
			points.push(new Vector2(361.4, 449.1));
			points.push(new Vector2(363.95, 453));
			points.push(new Vector2(368.2, 457.2));
			points.push(new Vector2(372.9, 461));
			points.push(new Vector2(410.2, 461));
			points.push(new Vector2(412.55, 449.1));
			points.push(new Vector2(417.4, 441.05));
			points.push(new Vector2(419.7, 432.9));
			points.push(new Vector2(422.05, 432.9));
			points.push(new Vector2(425.45, 424.8));
			points.push(new Vector2(428.8, 422.35));
			points.push(new Vector2(433.45, 416.65));
			points.push(new Vector2(438.25, 415.15));
			points.push(new Vector2(442.6, 412.4));
			points.push(new Vector2(447.4, 412.4));
			points.push(new Vector2(448.8, 416.65));
			points.push(new Vector2(454.55, 430.55));
			points.push(new Vector2(455.5, 434.8));
			points.push(new Vector2(459.25, 438.6));
			points.push(new Vector2(462.6, 440.9));
			points.push(new Vector2(466, 444.85));
			points.push(new Vector2(468.35, 452.9));
			points.push(new Vector2(475.55, 457.3));
			points.push(new Vector2(484.7, 457.3));
			points.push(new Vector2(494.7, 458.2));
			points.push(new Vector2(503.75, 461.1));
			points.push(new Vector2(522.2, 461.1));
			points.push(new Vector2(524.75, 453));
			points.push(new Vector2(527.1, 441.05));
			points.push(new Vector2(527.1, 432.9));
			points.push(new Vector2(531.9, 432.9));
			points.push(new Vector2(534.15, 424.8));
			points.push(new Vector2(538.6, 420.5));
			points.push(new Vector2(540.9, 416.65));
			points.push(new Vector2(542.35, 412.5));
			points.push(new Vector2(545.7, 408));
			points.push(new Vector2(550.45, 408));
			points.push(new Vector2(552.85, 398.1));
			points.push(new Vector2(554.75, 389.95));
			points.push(new Vector2(559.55, 388));
			points.push(new Vector2(564.35, 391.9));
			points.push(new Vector2(573.35, 391.9));
			points.push(new Vector2(578.1, 388));
			points.push(new Vector2(579.55, 379.95));
			points.push(new Vector2(582.9, 369.4));
			points.push(new Vector2(587.75, 367.55));
			points.push(new Vector2(588.65, 363.8));
			points.push(new Vector2(592.05, 359.5));
			points.push(new Vector2(596.85, 355.55));		






			availableZones.push(new LandingZone(0, 4));
			availableZones.push(new LandingZone(13, 3));
			availableZones.push(new LandingZone(25, 4));	
			availableZones.push(new LandingZone(34, 4));	
			availableZones.push(new LandingZone(63, 5));	
			availableZones.push(new LandingZone(75, 4));	
			availableZones.push(new LandingZone(106, 5));	
			availableZones.push(new LandingZone(111, 2));	
			availableZones.push(new LandingZone(121, 5));	
			availableZones.push(new LandingZone(133, 2));	
			availableZones.push(new LandingZone(148, 3));	


			zoneCombis.push([2,3,7,9]);
			zoneCombis.push([7,8,9,10]);
			zoneCombis.push([2,3,7,9]);
			zoneCombis.push([1,4,7,9]);
			zoneCombis.push([0,5,7,9]);
			zoneCombis.push([6,7,8,9]);
			zoneCombis.push([1,4,7,9]);	





		}


	}; 

	function LandscapeLine(p1, p2) { 
		this.p1 = p1; 
		this.p2 = p2; 
		this.landable = (p1.y==p2.y); 
		this.multiplier = 1; 

	}

	function LandingZone(linenum, multi) {

		this.lineNum = linenum; 
		this.multiplier = multi; 
	}

	var audioLibParams = {
		explosion :  ["noise",1.0000,0.6610,0.0000,0.0300,2.1960,2.0000,20.0000,440.0000,2000.0000,0.0000,0.0000,0.0000,7.9763,0.0003,0.0000,0.0000,0.1000,0.0000,0.0000,0.0000,-0.0100,0.0260,0.3210,1.0000,1.0000,0.0000,0.0000],
		thruster : ["noise",0.0000,1.0000,0.0000,2.0000,0.0000,0.0000,20.0000,281.0000,2400.0000,0.0000,0.0000,0.0000,7.9763,0.0003,0.0000,0.0000,0.0000,0.2515,0.0000,0.2544,0.0000,0.0000,0.2730,0.0000,0.3790,0.0000,0.0000],
		beep :  ["square",0.0000,0.030,0.0000,0.3000,0.0000,0.0000,20.0000,1210.0000,20.0000,0.0000,0.0000,0.0000,7.9763,0.0003,0.0000,0.0000,0.1000,0.0000,0.0000,0.4632,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000]

	};

	var samples = jsfxlib.createWaves(audioLibParams);
	//samples.test.play();
	//samples.explosion.play();
	//samples.thruster.loop = true; 
	//samples.thruster.play(); 

	var thrustSound = samples.thruster; 
	var thrustInterval = 0; 
	var thrustPlaying = false; 
	var thrustVolume = 0; 
	var thrustTargetVolume = 0 ; 

	//playThruster();

	function setThrustVolume(vol) { 
		thrustTargetVolume = vol; 
		if((vol>0) && (!thrustPlaying)) {
			playThruster(); 
		}


	}

	function playThruster() { 
		if(thrustInterval) clearInterval(thrustInterval); 
		if(thrustPlaying) {
			thrustSound.pause(); 

		}

		thrustSound.play(); 
		thrustSound.currentTime =0; 
		thrustInterval = setInterval(updateThruster, 10); 
		thrustPlaying = true;
	}

	function updateThruster(e) { 
		if(thrustSound.currentTime>1.5) thrustSound.currentTime=0.1; 
		if(thrustVolume!=thrustTargetVolume){
			thrustVolume+=((thrustTargetVolume-thrustVolume)*0.1); 
			if(Math.abs(thrustVolume-thrustTargetVolume)<0.01) 
				thrustVolume = thrustTargetVolume; 

			thrustSound.volume = thrustVolume; 
		}
		if(thrustVolume<=0) stopThruster(); 

	}
	function stopThruster() {

		if(!thrustPlaying) return; 

		//thrustSound.currentTime = 2.5; 
		clearInterval(thrustInterval); 
		thrustPlaying = false; 


	}


	KeyTracker = new (function(){

		this.keysPressed = {}; 
		this.UP = 38; 
		this.LEFT = 37; 
		this.RIGHT = 39; 
		this.DOWN = 40; 
		this.keyListeners = []; 

		this.isKeyDown = function (key) { 
			if (typeof key == 'string')
				key = key.charCodeAt(0); 
			return(this.keysPressed[key]);
		};

		document.addEventListener("keydown", function(e) {	

			KeyTracker.keysPressed[e.keyCode] = true; 

		}); 
		document.addEventListener("keyup", 	function(e) {
			KeyTracker.keysPressed[e.keyCode] = false;}); 

			this.addKeyDownListener = function(key, func) { 
				if (typeof key == 'string')
					key = key.charCodeAt(0); 

				this.keyListeners.push({key:key, func:func});

			}

	})();


	/*
	 *
	 * GAME JS
	 * 
	 */

	// screen size variables
	var SCREEN_WIDTH = window.innerWidth,
	SCREEN_HEIGHT = window.innerHeight,
	HALF_WIDTH = window.innerWidth / 2,
	HALF_HEIGHT = window.innerHeight / 2,
	touchable = "ontouchstart" in window,
	fps = 60,
	mpf = 1000/fps,
	counter = 0,
	gameStartTime = Date.now(),
	skippedFrames;


	// game states
	var	WAITING = 0,
	PLAYING = 1,
	LANDED = 2,
	CRASHED = 3,
	GAMEOVER = 4,

	gameState = GAMEOVER,

	score = 0,
	time = 0,

	lander = new Lander(),
	landscape = new Landscape(),
	players = {},



	// canvas element and 2D context
	canvas = document.createElement( 'canvas' ),
	context = canvas.getContext( '2d' ),

	// to store the current x and y mouse position
	mouseX = 0, mouseY = 0,

	stats = new Stats(),

	// to convert from degrees to radians,
	// multiply by this number!
	TO_RADIANS = Math.PI / 180,

	view = {x:0,
		y:0,
		scale :1,
		left :0,
		right : 0,
		top :0,
		bottom:0},
		zoomedIn = false,
		zoomFactor = 4;

		window.addEventListener("load", init);


		function init()
		{
			window.agcBody = 'function AGC (instruments, terrain) {return [Math.random()>0.7, Math.random()>0.5 ? -1:1];}';
			// CANVAS SET UP

			document.body.appendChild(canvas);

			stats.domElement.style.position = 'absolute';
			stats.domElement.style.top = (SCREEN_HEIGHT-45)+'px';
			//document.body.appendChild( stats.domElement );

			infoDisplay = new InfoDisplay(SCREEN_WIDTH, SCREEN_HEIGHT);
			document.body.appendChild(infoDisplay.domElement);

			canvas.width = SCREEN_WIDTH;
			canvas.height = SCREEN_HEIGHT;


			document.body.addEventListener('mousedown', onMouseDown);
			document.body.addEventListener('touchstart', onTouchStart);

			window.addEventListener('resize', resizeGame);
			window.addEventListener('orientationchange', resizeGame);


			resizeGame();
			restartLevel();

			infoDisplay.updateFW.domElement.onclick = function () {
				agcBody = window.prompt('AGC = ', agcBody );
				eval(agcBody);
				window.AGC = AGC;
			};

			loop();

			lander.pos.x = 110;
			newGame();
		}

		function sendPosition() {
			if(gameState==PLAYING) {
				var update = {
					type : 'update',
					x : Math.round(lander.pos.x*100),
					y : Math.round(lander.pos.y*100),
					a : Math.round(lander.rotation),
					t : lander.thrusting ? 1 :0
				}
			}
		}


		//


		function loop() {
			var orders;
			requestAnimationFrame(loop);

			skippedFrames = 0;

			counter++;
			var c = context;

			var elapsedTime = Date.now() - gameStartTime;
			var elapsedFrames = Math.floor(elapsedTime/mpf);
			var renderedTime = counter*mpf;

			if(elapsedFrames<counter) {
				c.fillStyle = 'green';
				c.fillRect(0,0,10,10);
				counter--;
				return;
			}

			while(elapsedFrames > counter) {
				lander.update();
				if((counter%6)==0){
					sendPosition();

				}


				counter++;

				skippedFrames ++;
				if (skippedFrames>30) {
					//set to paused
					counter = elapsedFrames;
				}



			}

			//stats.update();


			if(gameState == PLAYING) {
				//checkKeys();
				orders = AGC({
					x: lander.left+6,
					r: lander.right,
					a: lander.altitude,
					h: lander.vel.x*200,
					v: lander.vel.y*200,
					o: lander.rotation, 
				}, terrain);
				lander.thrust(orders[0]);
				if(orders[1]) lander.rotate(orders[1]>0 ? 1 : -1);	
			}

			lander.update();
			if((counter%6)==0){
				sendPosition();

			}

			if((gameState == WAITING) && (lander.altitude<100) ) {
				gameState=GAMEOVER;
				restartLevel();
			}

			if((gameState== PLAYING) || (gameState == WAITING))
				checkCollisions();

			updateView();
			render();

		}

		function render() {

			var c = context;

			//c.fillStyle="rgb(0,0,0)";
			c.clearRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
			//c.fillRect(lander.left, lander.top, lander.right-lander.left, lander.bottom-lander.top);
			if(skippedFrames>0) {
				c.fillStyle = 'green';
				c.fillRect(0,0,10*skippedFrames,10);
			}
			c.save();
			c.translate(view.x, view.y);
			c.scale(view.scale, view.scale);

			// THIS CODE SHOWS THE VIEWPORT
			// c.beginPath();
			// 	c.moveTo(view.left+2, view.top+2);
			// 	c.lineTo(view.right-2, view.bottom-2);
			// 	c.strokeStyle = 'blue';
			// 	c.lineWidth = 2;
			// 	c.rect(view.left, view.top, view.right-view.left, view.bottom-view.top);
			// 	c.stroke();
			//

			landscape.render(context, view);
			lander.render(context, view.scale);

			for (var id in players) {
				var player = players[id];
				player.render(context, view.scale);
			}

			if(counter%4==0) updateTextInfo();

			c.restore();


		}

		function checkKeys() {

			if(KeyTracker.isKeyDown(KeyTracker.LEFT)) {
				lander.rotate(-1);
			} else if(KeyTracker.isKeyDown(KeyTracker.RIGHT)) {
				lander.rotate(1);
			}

			if(KeyTracker.isKeyDown(KeyTracker.UP)) {
				lander.thrust(true);
			} else {
				lander.thrust(false);
			}

			// SPEED MODE!
			if(KeyTracker.isKeyDown('S')) {
				for(var i=0; i<3;i++) lander.update();
			}

		}


		function updateView()
		{
			var zoomamount  = 0,
			marginx  = SCREEN_WIDTH *0.2,
			margintop = SCREEN_HEIGHT * 0.2,
			marginbottom = SCREEN_HEIGHT * 0.3;

			if((!zoomedIn) && (lander.altitude < 60)) {
				setZoom(true);
			} else if((zoomedIn) && (lander.altitude > 160)) {
				setZoom(false);
			}

			zoomamount = view.scale;


			if(((lander.pos.x * zoomamount) + view.x < marginx)){
				view.x = -(lander.pos.x * zoomamount) + marginx;
			} else if (((lander.pos.x * zoomamount) + view.x > SCREEN_WIDTH - marginx)) {
				view.x = -(lander.pos.x * zoomamount) + SCREEN_WIDTH - marginx;
			}

			if((lander.pos.y * zoomamount) + view.y < margintop) {
				view.y = -(lander.pos.y * zoomamount) + margintop;
			} else if ((lander.pos.y * zoomamount) + view.y > SCREEN_HEIGHT - marginbottom) {
				view.y = -(lander.pos.y * zoomamount) + SCREEN_HEIGHT - marginbottom;
			}


			view.left = -view.x/view.scale;
			view.top = -view.y/view.scale;
			view.right = view.left + (SCREEN_WIDTH/view.scale);
			view.bottom = view.top + (SCREEN_HEIGHT/view.scale);


		}



		function setLanded(line) {

			multiplier = line.multiplier;

			lander.land();

			var points = 0;
			if(lander.vel.y<0.04) {
				points = 50 * multiplier;
				// show message - "a perfect landing";
				infoDisplay.showGameInfo("CONGRATULATIONS<br>A PERFECT LANDING\n" + points + " POINTS");

			} else {
				points = 15 * multiplier;
				// YOU LANDED HARD
				infoDisplay.showGameInfo("YOU LANDED HARD<br>YOU ARE HOPELESSLY MAROONED<br>" + points + " POINTS");
				lander.makeBounce();
			}

			score+=points;
			// TODO Show score
			gameState = LANDED;
			scheduleRestart();
		}

		function setCrashed() {
			lander.crash();

			if(lander.fuel<=0) {
				gameState = GAMEOVER;
				// show out of fuel message
				infoDisplay.showGameInfo("OUT OF FUEL<br><br>GAME OVER");
			} else {
				// show crashed message
				// subtract fuel

				var rnd  = Math.random();
				var msg ='';
				if(rnd < 0.3){
					msg = "YOU JUST DESTROYED A 100 MEGABUCK LANDER";
				} else  if(rnd < 0.6){
					msg = "DESTROYED";
				} else {
					msg = "YOU CREATED A TWO MILE CRATER";
				}
				var fuellost = 300; //Math.round(((Math.random() * 200) + 200));
				msg = "AUXILIARY FUEL TANKS DESTROYED<br>" + fuellost + " FUEL UNITS LOST<br><br>" + msg;

				lander.fuel -= fuellost;



				if(lander.fuel<=0) {
					gameState = GAMEOVER;
				} else {
					gameState = CRASHED;
				}

				infoDisplay.showGameInfo(msg);
			}
			scheduleRestart();

			samples.explosion.play();
		}

		function onMouseDown(e) {
			e.preventDefault();
			if(gameState==WAITING) newGame();
		}

		function onTouchStart(e) {
			e.preventDefault();
			if(gameState==WAITING) newGame();

		}

		window.newGame = function newGame() {

			lander.fuel = 3000;

			time = 0;
			score = 0;

			gameStartTime = Date.now();
			counter = 0;

			landscape.resetCombi();
			restartLevel();

		}

		function scheduleRestart() {
			setTimeout(restartLevel,4000);

		}
		function restartLevel() {
			var oldX = lander.pos.x;
			lander.reset();
			if(oldX) lander.pos.x = oldX;
			landscape.setZones();
			makeTerrain();
			setZoom(false);

			if(gameState==GAMEOVER) {
				gameState = WAITING;
				showStartMessage();
				lander.vel.x = 2;
				//initGame();
			} else {
				gameState = PLAYING;
				infoDisplay.hideGameInfo();
			}


		}
		function checkCollisions() {

			var lines = landscape.lines,
			right = lander.right%landscape.tileWidth,
			left = lander.left%landscape.tileWidth;

			while(right<0){
				right+=landscape.tileWidth;
				left += landscape.tileWidth;
			}

			for(var i=0; i<lines.length; i++ ) {
				line = lines[i];
				line.checked = false;
				// if the ship overlaps this line
				if(!((right<line.p1.x) || (left>line.p2.x))){

					lander.altitude = line.p1.y-lander.bottom;

					// if the line's horizontal
					if(line.landable) {
						line.checked = true;
						// and the lander's bottom is overlapping the line
						if(lander.bottom>=line.p1.y) {
							console.log('lander overlapping ground');
							// and the lander is completely within the line
							if((left>line.p1.x) && (right<line.p2.x)) {
								//console.log('lander within line', lander.rotation, lander.vel.y);
								// and we're horizontal and moving slowly
								if((lander.rotation==0) && (lander.vel.y<0.07)) {
									console.log('horizontal and slow');
									setLanded(line);
								} else {
									setCrashed();
								}
							} else {
								// if we're note within the line
								setCrashed();
							}
						}
						// if lander's bottom is below either of the two y positions
					} else if(( lander.bottom > line.p2.y) || (lander.bottom > line.p1.y)) {

						if( pointIsLessThanLine(lander.bottomLeft, line.p1, line.p2) ||
							 pointIsLessThanLine(lander.bottomRight, line.p1, line.p2)) {

							setCrashed();
						}
					}
				}
			}

		};


		function pointIsLessThanLine(point, linepoint1, linepoint2) {

			// so where is the y of the line at the point of the corner?
			// first of all find out how far along the xaxis the point is
			var dist = (point.x - linepoint1.x) / (linepoint2.x - linepoint1.x);
			var yhitpoint  = linepoint1.y + ((linepoint2.y - linepoint1.y) * dist);

			return ((dist > 0) && (dist < 1) && (yhitpoint <= point.y)) ;
		}

		function updateTextInfo() {

			infoDisplay.updateBoxInt('score', score, 4);
			infoDisplay.updateBoxInt('fuel', lander.fuel, 4);
			if(gameState == PLAYING) infoDisplay.updateBoxTime('time', counter*mpf);

			infoDisplay.updateBoxInt('alt', (lander.altitude<0) ? 0 : lander.altitude, 4);
			infoDisplay.updateBoxInt('horizSpeed', (lander.vel.x*200));
			infoDisplay.updateBoxInt('vertSpeed', (lander.vel.y*200));

			infoDisplay.updateBoxInt('thrust', lander.thrusting);
			infoDisplay.updateBoxInt('rotate', lander.doRotation);

			// +(lander.vel.x<0)?' ‹':' ›'
			// +(lander.vel.y<0)?' ˆ':' >'

			if((lander.fuel < 300) && (gameState == PLAYING)) {
				if((counter%50)<30) {
					var playBeep;
					if(lander.fuel <= 0) {
						playBeep = infoDisplay.showGameInfo("Out of fuel");
					} else {
						playBeep = infoDisplay.showGameInfo("Low on fuel");
					}
					if(playBeep) samples.beep.play();
				} else {
					infoDisplay.hideGameInfo();
				}


			}

		}

		function showStartMessage() {
			infoDisplay.showGameInfo("INSERT COINS<br><br>CLICK TO PLAY<br>ARROW KEYS TO MOVE");
		}

		function setZoom(zoom )
		{
			if(zoom){
				view.scale = SCREEN_HEIGHT/700*6;
				zoomedIn = true;
				view.x = -lander.pos.x * view.scale + (SCREEN_WIDTH / 2);
				view.y = -lander.pos.y * view.scale + (SCREEN_HEIGHT * 0.25);
				lander.scale = 0.25;

			}
			else {

				view.scale = SCREEN_HEIGHT/700;
				zoomedIn = false;
				lander.scale = 0.6;
				view.x = 0;
				view.y = 0;
			}

			for (var id in players) {
				var player = players[id];
				player.scale = lander.scale;
			}


		}

		// returns a random number between the two limits provided
		function randomRange(min, max){
			return ((Math.random()*(max-min)) + min);
		}

		function clamp(value, min, max) {
			return (value<min) ? min : (value>max) ? max : value;
		}

		function map(value, min1, max1, min2, max2) {
			return (((value-min1)/(max1-min1)) * (max2-min2))+min2;
		}


		function onDocumentMouseMove( event )
		{
			mouseX = ( event.clientX - HALF_WIDTH );
			mouseY = ( event.clientY - HALF_HEIGHT );
		}

		function resizeGame (event) {

			var newWidth = window.innerWidth;
			var newHeight = window.innerHeight;

			if((SCREEN_WIDTH== newWidth) && (SCREEN_HEIGHT==newHeight)) return;
			if(touchable) window.scrollTo(0,-10);

			SCREEN_WIDTH = canvas.width = newWidth;
			SCREEN_HEIGHT = canvas.height = newHeight;

			setZoom(zoomedIn )
			stats.domElement.style.top = (SCREEN_HEIGHT-45)+'px';
			infoDisplay.arrangeBoxes(SCREEN_WIDTH, SCREEN_HEIGHT);

		}

		window.lander = lander;
		window.landscape = landscape;	

		var terrain = window.terrain = [];
		function makeTerrain() {
			var pp, cp = landscape.points[0];
			for(var i = 0 ; i < landscape.lines.length; i++) {
				pp = landscape.lines[i].p1;
				cp = landscape.lines[i].p2;
				var slope = (pp.y - cp.y) / (pp.x - cp.x)
				for(var x = Math.floor(pp.x); x < cp.x; x++) {
					window.terrain[x] = {
						h: pp.y + slope * (x-pp.x),
						p: landscape.lines[i].multiplier * landscape.lines[i].landable,
					}	
				}
			}
		}


}).call({});

function AGC (instruments, terrain) {
	// engine, turn
	return [Math.random()>0.7, Math.random()>0.5 ? -1:1];
}


