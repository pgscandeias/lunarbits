function AGC (instruments, terrain) {
    /* Simulation parameters */
    xConstant = 0.005;
    gAccel = 0.1*xConstant;
    xDrag = 0.9997;
    hCanvas = 646;


    /* Convenience */

    i = instruments;
    rotation = 0;
    thrust = 0;
    terrainMultiplier = Math.ceil(i.x / 900) - 1;


    /* Instrumentation */

    function getTerrain(x) {
        factor = terrainMultiplier;
        return Math.floor(x - 900 * factor);
    }

    function lookDown() {
        t = terrain[getTerrain(i.x)];
        t.a = hCanvas - t.h;
        return t;
    }

    abs = {
        a: i.a + lookDown().a,
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o),
        x: getTerrain(i.x)
    };

    function getFramesLeftToDrop(targetA) {
        /* High school maths teachers are RIGHT! */
        a = gAccel / 2;
        b = i.v * xConstant;
        c = abs.a - targetA;
        f = (b*-1 + Math.sqrt(b*b + 4*a*c)) / (2 * a);

        return f;
    }

    function predictX(frames) {
        return i.h * xConstant * frames;
    }

    function predictLandingX(target) {
        return abs.x + predictX(getFramesLeftToDrop(target.a));
    }

    function getTargetOvershoot(target) {
        x = predictLandingX(target);
        return x - target.x;
    }

    function getGlideProfile(target, tolerance) {
        overshoot = getTargetOvershoot(target);
        if (overshoot > tolerance) return 'long';
        else if (overshoot < tolerance*-1) return 'short';
        else return 'good';
    }


    /* Target selection */
    tgtT = 700;
    target = {
        x: tgtT,
        a: hCanvas - terrain[tgtT].h
    };


    /* Controls */
    glideProfile = getGlideProfile(target, 20);
    console.log('Gonna be '+glideProfile);
    

    /* Punch it, Jeb! */
    return [thrust, rotation];
}