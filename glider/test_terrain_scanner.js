function AGC (instruments, terrain) {
    i = instruments;
    abs = {
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };
    rotation = 0;
    thrust = 0;

    function terrainX(x) {
        factor = Math.ceil(x / 900) - 1;
        return x - 900 * factor;
    }

    function scan() {
        x = i.x;
        n = 0;
        found = false;

        while (!found) {
            n+=2;
            factor = (n/2) % 2 == 0 ? 1 : -1;
            
            x = x + n * factor;
            tx = terrainX(Math.round(x));
            
            if (terrain[tx] && terrain[tx].p > 2) {
                found = true;
                return {
                    x: tx + 2,
                    p: terrain[tx].p,
                    h: terrain[tx].h
                };
            }
        }
    }
    target = scan();
    i.distance = Math.round(target.x - i.x);
    abs.distance = Math.abs(i.distance);

    function cruise(x) {
        minH = 50;
        maxH = abs.distance / 2;

        if (abs.h < minH) boost();
        else if (abs.h > maxH) brake();
        else upright();
        
        hover();
    }

    function boost() {
        signal = target.x - i.x > 0;
        rotation = 1 * signal;
        thrust = 1;
    }
    function brake() {
        signal = target.x - i.x < 0;
        rotation = 1 * signal;
        thrust = 1;
    }

    function upright() {
        if (abs.o < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }

    function hover() {
        if (i.v < 0) thrust = 0;
        if (i.v > 0) {
            thrust = 1;
            if (Math.abs(i.o) > 45) rotation = i.o > 0 ? -1 : 1;
        }
    }

    cruise(2700);
    console.log('x: '+i.x+' means: '+terrainX(i.x));

    return [thrust, rotation];
}