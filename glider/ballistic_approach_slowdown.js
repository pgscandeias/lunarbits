function AGC (instruments, terrain) {
    /* Simulation parameters */
    xConstant = 0.005;
    gAccel = 0.1 * xConstant;
    xDrag = 0.9997;
    hCanvas = 646;

    /* Instrumentation and convenience */

    i = instruments;
    i.hSignal = i.h > 0 ? 1 : -1;
    rotation = 0;
    thrust = 0;
    terrainMultiplier = Math.ceil(i.x / 900) - 1;

    function getTerrain(x) {
        factor = terrainMultiplier;
        return Math.floor(x - 900 * factor);
    }

    function lookDown() {
        t = terrain[getTerrain(i.x)];
        t.a = hCanvas - t.h;
        return t;
    }

    abs = {
        a: i.a + lookDown().a,
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o),
        x: getTerrain(i.x)
    };

    function getFramesLeftToDrop(targetA) {
        /* High school maths teachers are RIGHT! */
        a = gAccel / 2;
        b = i.v * xConstant;
        c = abs.a - targetA;
        f = (b*-1 + Math.sqrt(b*b + 4*a*c)) / (2 * a);

        return f;
    }

    function predictX(frames) {
        return i.h * xConstant * frames;
    }

    function predictLandingX(target) {
        return i.x + predictX(getFramesLeftToDrop(target.a));
    }

    function getTargetOvershoot(target) {
        x = predictLandingX(target);
        return x - target.x;
    }

    function getGlideProfile(target, tolerance) {
        overshoot = getTargetOvershoot(target);
        if (Math.abs(overshoot) > tolerance) {
            if (overshoot > 0) return 'right';
            else return 'left';
        }
        else return 'bullseye';
    }

    function getGlideTolerance(target) {
        if (i.range > 200) return 20;
        else if (i.range > 100) return 10;
        else return 1;
    }

    function getMinH() {
        if (abs.distance > 10) return Math.abs(abs.distance / 3) * target.o;
        else return 1;
    }

    function getMaxV() {
        maxV = i.a * 2/3;
        return maxV > 7 ? maxV : 7;
    }

    function getRange(target) {
        return Math.sqrt(Math.pow(target.x - abs.x, 2) + Math.pow(target.a - abs.a, 2));
    }


    /* Target selection */
    tgtT = 770;
    target = {
        x: tgtT,
        a: hCanvas - terrain[tgtT].h,
        o: tgtT > abs.x ? 1 : -1,
        distance: tgtT - i.x
    };
    i.range = getRange(target);
    abs.distance = Math.abs(target.x - abs.x);

    /* Controls */
    function upright() {
        if (abs.o < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }
    boost = '';
    function boostUp() {
        upright();
        thrust = 1;
        boost = 'GO UP';
    }

    function boostRight() {
        rotation = 1;
        thrust = 1;
        boost = 'GO RIGHT';
    }

    function boostLeft() {
        rotation = -1;
        thrust = 1;
        boost = 'GO LEFT!';
    }
    

    /* Default attitude */
    upright();

    /* Range control */
    glideProfile = getGlideProfile(target, getGlideTolerance(target));
    if (glideProfile == 'left') {
        if (i.hSignal > 0 && i.h > getMinH()) boostUp();
        else boostRight();
    }
    else if (glideProfile == 'right') {
        if (i.hSignal < 0 && i.h < getMinH()) boostUp();
        else boostLeft();
    }

    /* Descent control */
    if (i.v > getMaxV()) boostUp();

    /* Landing attitude */
    if (i.a < 0.5) upright();

    console.log(
        ' x: '+Math.round(abs.x)+
        ' t: '+target.x+
        ' r: '+getRange(target)+
        ' Gonna land '+glideProfile+
        ' '+boost
    );
    

    /* Punch it, Jeb! */
    return [thrust, rotation];
}