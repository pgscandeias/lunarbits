function AGC (instruments, terrain) {
    /* Convenience */
    i = instruments;
    var terrainMultiplier = Math.ceil(i.x / 900) - 1;
    function getTerrain(x) {
        var factor = terrainMultiplier;
        return Math.floor(x - 900 * factor);
    }
    function lookDown() {
        t = terrain[getTerrain(i.x)];
        t.a = 646 - t.h;
        return t;
    }
    
    abs = {
        a: i.a + lookDown().a),
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };
    rotation = 0;
    thrust = 0;

    function upright() {
        if (abs.o < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }

    function hover(tolerance) {
        if (i.v < tolerance) thrust = 0;
        if (i.v > tolerance) {
            thrust = 1;
            if (Math.abs(i.o) > 45) rotation = i.o > 0 ? -1 : 1;
        }
    }

    upright();
    hover(0);

    console.log(
        "\n---------------------"+
        "\ni.a:   "+i.a+
        "\nt.h:   "+lookDown().a+
        "\nabs.a: "+abs.a
    );

    /* Output */
    return [thrust, rotation];
}