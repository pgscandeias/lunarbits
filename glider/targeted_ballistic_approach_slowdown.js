function AGC (instruments, terrain) {
    /* Simulation parameters */
    xConstant = 0.005;
    gAccel = 0.1 * xConstant;
    xDrag = 0.9997;
    hCanvas = 646;

    /* Instrumentation and convenience */

    i = instruments;
    i.hSignal = i.h > 0 ? 1 : -1;
    i.x -= 8;
    rotation = 0;
    thrust = 0;
    terrainMultiplier = Math.ceil(i.x / 900) - 1;

    function getTerrainX(x) {
        factor = terrainMultiplier;
        return Math.floor(x - 900 * factor);
    }

    function getTerrainData(x) {
        x = getTerrainX(x);
        t = terrain[x];
        t.x = x;
        t.a = hCanvas - t.h;
        return t;
    }

    function lookDown() {
        t = terrain[getTerrainX(i.x)];
        t.a = hCanvas - t.h;
        return t;
    }

    abs = {
        a: i.a + lookDown().a,
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o),
        x: getTerrainX(i.x)
    };

    function getFramesLeftToDrop(targetA) {
        /* High school maths teachers are RIGHT! */
        a = gAccel / 2;
        b = i.v * xConstant;
        c = abs.a - targetA;
        f = (b*-1 + Math.sqrt(b*b + 4*a*c)) / (2 * a);

        return f;
    }

    function predictX(frames) {
        return i.h * xConstant * frames;
    }

    function predictLandingX(target) {
        return i.x + predictX(getFramesLeftToDrop(target.a));
    }

    function getTargetOvershoot(target) {
        x = predictLandingX(target);
        return x - target.x;
    }

    function getGlideProfile(target, tolerance) {
        overshoot = getTargetOvershoot(target);
        if (Math.abs(overshoot) > tolerance) {
            if (overshoot > 0) return 'right';
            else return 'left';
        }
        else return 'bullseye';
    }

    function getGlideTolerance(target) {
        if (target.range > 100) return 10;
        else if (target.range > 10) return 1;
        else return 0.25;
    }

    function getMinH() {
        if (abs.distance > 30) return Math.abs(abs.distance / 3) * target.o;
        else return 1 * target.o;
    }

    function getMaxV() {
        targetA = abs.a - target.a;
        if (i.a < a) a = i.a;
        else a = targetA;

        maxV = a / 2;
        return maxV > 7 ? maxV : 7;
    }

    function getRange(target) {
        return Math.sqrt(Math.pow(target.x - abs.x, 2) + Math.pow(target.a - abs.a, 2));
    }


    /* Landing zone scanner */
    function getLzs() {
        lzs = [];
        j = 0;
        while (terrain[j]) {
            if (terrain[j].p > 2) {
                padScore = terrain[j].p;
                left = j;
                while (terrain[j].p == padScore) j++;
                right = j;
                width = right - left;
                targetX = left;

                if (width > 3) { /* lander must fit comfortably */
                    t = getTerrainData(targetX);
                    lzs.push({
                        l: left,
                        r: right,
                        x: targetX,
                        p: padScore,
                        w: width,
                        a: t.a,
                        range: getRange(t)
                    });
                }
            }
            else j++;
        }

        return lzs;
    }
    landingZones = getLzs();

    /* Target selection */
    lz = { range: 9999 };
    for (var z in landingZones) {
        if (landingZones[z].range < lz.range) lz = landingZones[z];
    }
    target = {
        x: lz.x,
        a: lz.a,
        p: lz.p,
        o: lz.x > abs.x ? 1 : -1,
        left: lz.l,
        right: lz.r,
        distance: lz.x - i.x,
        range: lz.range
    };
    abs.distance = Math.abs(target.x - abs.x);

    /* Controls */
    function upright() {
        if (abs.o < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }
    boost = '';
    function boostUp() {
        upright();
        thrust = 1;
        boost = 'GO UP';
    }

    function boostRight() {
        rotation = 1;
        if (i.o >= 45) thrust = 1;
        boost = 'GO RIGHT';
    }

    function boostLeft() {
        rotation = -1;
        if (i.o <= -45) thrust = 1;
        boost = 'GO LEFT!';
    }
    

    /* Default attitude */
    upright();

    /* Glide control */
    glideProfile = getGlideProfile(target, getGlideTolerance(target));
    minH = getMinH();
    if (i.h > 0) { /* Moving right */
        if (glideProfile == 'left') {
            if (i.h > minH && i.v > 0) boostUp();
            else boostRight();
        }
        else if (glideProfile == 'right') boostLeft();
    }
    else if (i.h < 0) { /* Moving left */
        if (glideProfile == 'right') {
            if (i.h < minH && i.v > 0) boostUp();
            else boostLeft();
        }
        else if (glideProfile == 'left') boostRight();
    }

    /* Descent control */
    if (i.v > getMaxV()) boostUp();

    /* Landing position */
    if (i.a < 0.75) upright();


    function logTrajectory() {
        console.log(
            ' x: '+Math.round(abs.x)+
            ' t: '+target.x+
            ' o: '+Math.round(getTargetOvershoot(target))+
            ' i.h: '+i.h+
            ' Gonna land '+glideProfile+
            ' '+boost
        );
    }

    function logLanding() {
        console.log(
            '\n-------------'+
            '\ni.x:  '+i.x+
            '\ni.r:  '+i.r+
            '\nlz.l: '+target.left+
            '\nlz.r: '+target.right+
            '\nt.x:  '+target.x
        );
    }
    
    /*
    if (i.a < 50) logLanding();
    else logTrajectory();
    */
    

    /* Punch it, Jeb! */
    return [thrust, rotation];
}