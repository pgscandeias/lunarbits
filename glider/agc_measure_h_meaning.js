function AGC (instruments, terrain) {
    /* What does the horizontal speed reading mean? */

    i = instruments;
    abs = {
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };
    rotation = 0;
    thrust = 0;

    if (!window.ship) window.ship = {};
    if (!window.ship.frame){
        window.ship.frame = 0;
        window.ship.x = i.x;
        window.ship.speedH = i.h;
    }
    else if(window.ship.frame == 1) {
        window.ship.speedH += i.h;
        avgSpeed = window.ship.speedH / 2;
        distanceTraveled = i.x - window.ship.x;

        /*console.log('Δt=1; Δd='+distanceTraveled+'; ~s='+avgSpeed);*/

    }
    window.ship.frame++;
    
    console.log(window.ship.frame+": "+i.a + terrain[Math.round(i.x)].h);
    
    if (!window.ship.prevV ) window.ship.prevV = i.h;

    if (Math.abs(i.o) < 10) rotation = 0;
    else rotation = i.o > 0 ? -1 : 1;


    return [thrust, rotation];
}