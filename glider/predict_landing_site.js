function AGC (instruments, terrain) {
    /* Simulation parameters */
    xConstant = 0.005;
    gAccel = 0.1*xConstant;
    xDrag = 0.9997;

    /* Convenience and instrumentation */
    i = instruments;
    rotation = 0;
    thrust = 0;
    terrainMultiplier = Math.ceil(i.x / 900) - 1;
    function getTerrain(x) {
        factor = terrainMultiplier;
        return Math.floor(x - 900 * factor);
    }
    function lookDown() {
        t = terrain[getTerrain(i.x)];
        t.a = 646 - t.h;
        return t;
    }
    abs = {
        a: i.a + lookDown().a,
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };

    function getFramesLeftToDrop(targetA) {
        /* High school maths teachers are RIGHT! */
        a = gAccel / 2;
        b = i.v * xConstant;
        c = abs.a - targetA;
        f = (b*-1 + Math.sqrt(b*b + 4*a*c)) / (2 * a);

        return f;
    }

    function predictX(frames) {
        return abs.h * xConstant * frames;
    }

    function predictLandingX(target) {
        return predictX(getFramesLeftToDrop(target.a));
    }

    
    if (!window.ship) {
        window.ship = {
            framesElapsed: 0,
        };
    }
    framesLeft = getFramesLeftToDrop(0);
    if (window.ship.framesElapsed == 1) {
        window.ship.framesPredicted = framesLeft;
        window.ship.Xpredicted = predictX(framesLeft);
    }
    window.ship.framesElapsed++;


    console.log('\n-----------------------');
    console.log(
        '\nFRAMES'+
        '\npredicted: '+window.ship.framesPredicted+
        '\nrealtime: '+getFramesLeftToDrop(0)+
        '\nelapsed: '+window.ship.framesElapsed
    );
    console.log(
        '\nX'+
        '\npredicted: '+window.ship.Xpredicted+
        '\n(real time): '+predictX(getFramesLeftToDrop(0))+
        '\nactual: '+i.x
    );

    /* Punch it, Jeb! */
    return [thrust, rotation];
}