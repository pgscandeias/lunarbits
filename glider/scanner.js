function AGC (instruments, terrain) {
    /* Convenience and instrumentation */
    i = instruments;
    rotation = 0;
    thrust = 0;
    terrainMultiplier = Math.ceil(i.x / 900) - 1;
    function getTerrain(x) {
        factor = terrainMultiplier;
        return Math.floor(x - 900 * factor);
    }
    function lookDown() {
        t = terrain[getTerrain(i.x)];
        t.a = 646 - t.h;
        return t;
    }
    abs = {
        a: i.a + lookDown().a,
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };

    /* Scanner */
    function getLzs() {
        lzs = [];

        j = 0;
        while (terrain[j]) {
            if (terrain[j].p > 0) {
                score = terrain[j].p;
                left = j;
                while (terrain[j].p == score) j++;
                right = j - 1;
                width = right - left;
                targetX = left + 1;

                if (width > 3) { /* lander must fit comfortably */
                    lzs.push({
                        l: left,
                        r: right,
                        x: targetX,
                        p: score,
                        w: width
                    });
                }
            }
            else j++;
        }

        return lzs;
    }
    landingZones = getLzs();

    if (!window.logged) {
        
        console.log(landingZones);
        for (var z in landingZones) {
            
            console.log(
                "\n---------------------"+
                "\np: "+landingZones[z].p+
                "\nw: "+landingZones[z].w+
                "\nl: "+landingZones[z].l+
                "\nr: "+landingZones[z].r+
                "\nx: "+landingZones[z].x
            );
        }

        window.logged = true;
    }

    /* Controls */
    function upright() {
        if (abs.o < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }

    function hover(tolerance) {
        if (i.v < tolerance) thrust = 0;
        if (i.v > tolerance) {
            thrust = 1;
            if (Math.abs(i.o) > 45) rotation = i.o > 0 ? -1 : 1;
        }
    }

    /* Decisions */
    upright();
    hover(0);


    /* Report
    console.log(
        "\n---------------------"+
        "\ni.a:   "+i.a+
        "\nt.h:   "+lookDown().a+
        "\nabs.a: "+abs.a
    );
    */


    /* Punch it, Jeb! */
    return [thrust, rotation];
}