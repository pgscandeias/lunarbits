function AGC (instruments, terrain) {
    var i = instruments;
    var abs = {
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };
    var rotation = 0;
    var thrust = 0;

    var terrainMultiplier = Math.ceil(i.x / 900) - 1;
    function terrainX(x) {
        var factor = terrainMultiplier;
        return x - 900 * factor;
    }

    function scan() {
        var x = i.x;
        var n = 0;
        var found = false;

        while (!found) {
            n+=2;
            var factor = (n / 2) % 2 == 0 ? 1 : -1;
            var tx = terrainX(Math.round(x));
            x = x + n * factor;
            
            if (terrain[tx] && terrain[tx].p > 3) {
                found = true;
                return {
                    x: tx + 2,
                    p: terrain[tx].p,
                    h: terrain[tx].h
                };
            }
        }
    }
    var target = scan();

    i.distance = Math.round(target.x - i.x);
    abs.distance = Math.abs(i.distance);

    function getGroundClearance() {
        toTarget = i.a - terrain[terrainX(Math.round(i.x))].h;
        toGround = i.a;

        if (toTarget < toGround) return toTarget;
        else return toGround;
    }

    function translate(x) {
        rotation = i.distance < 0 ? -1 : 1;
        direction = i.distance < 0 ? 'left' : 'right';
        thrust =  abs.h > 0 ? 1 : 0;

        /* Speed control */
        maxH = abs.distance / 2;
        if (maxH < 1) maxH = 1;
        speedLimit(maxH);

        /* Don't rise or fall */
        if (i.v < 0) thrust = 0;
        if (i.v > 0) {
            thrust = 1;
            if (Math.abs(i.o) > 45) rotation = i.o > 0 ? -1 : 1;
        }
    }

    function land() {
        if (getGroundClearance() < 20 && abs.distance > 1) {
            if (
                (i.h > 0 && i.distance > 0 && i.x > target.x) ||
                (i.h < 0 && i.distance < 0 && i.x < target.x)
            ) {
                translate(target.x);
            }

        } else {
            upright();
            thrust = 0;
            if (i.a < 10) maxV = 5;
            else maxV = i.a / 3;

            if (i.v > maxV) { thrust = 1; }
        }
        maxTilt(10);
    }
    
    function speedLimit(s) {
        if (abs.h > s) {
            throttle = 1;
            rotation = i.h > 0 ? -1 : 1;
        }
    }

    function throttleLimit(t) {
        if (abs.h > t && samesign(i.h, i.distance)) {
            thrust = 0;
        }
    }

    function upright() {
        if (abs.o < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }

    function maxTilt(t) {
        if (abs.o == t) rotation = 0;
        else if (abs.o >= t) rotation = i.o > 0 ? -1 : 1;
    }

    function samesign(a, b) {
        return ((a > 0 && b > 0) || (a < 0 && b < 0));
    }

    translate(target.x);
    if (abs.distance < 20 && abs.h < 5) land();


    function dashboard() {
        console.log(
            "\n-----------------"
            +"\nx: "+Math.round(i.x)
            +"\nt: "+Math.round(target.x)+" ("+Math.round(abs.distance)+")"
        );
    }
    dashboard();

    return [thrust, rotation];
}