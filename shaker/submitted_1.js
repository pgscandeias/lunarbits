function AGC (instruments, terrain) {
    i = instruments;
    abs = {
        h: Math.abs(i.h),
        v: Math.abs(i.v),
        o: Math.abs(i.o)
    };
    rotation = 0;
    thrust = 0;

    function scan() {
        x = i.x;
        n = 0;
        found = false;

        while (!found) {
            n+=2;
            factor = (n/2) % 2 == 0 ? 1 : -1;
            tx = Math.round(x);
            x = x + n * factor;
            
            if (terrain[tx] && terrain[tx].p > 2) {
                found = true;
                return {
                    x: tx + 2,
                    p: terrain[tx].p,
                    h: terrain[tx].h
                };
            }
        }
    }
    target = scan();
    i.distance = Math.round(target.x - i.x);
    abs.distance = Math.abs(i.distance);

    function translate(x) {
        rotation = i.distance < 0 ? -1 : 1;
        direction = i.distance < 0 ? 'left' : 'right';
        thrust =  abs.h > 0 ? 1 : 0;

        /* Speed control */
        if (abs.distance < 50) speedLimit(20);
        else throttleLimit(50);
        if (abs.h < 10) {
            if (abs.distance < 20) maxTilt(10);
            else maxTilt(20);
        }

        /* Don't rise or fall */
        if (i.v < 0) thrust = 0;
        if (i.v > 0) {
            thrust = 1;
            if (Math.abs(i.o) > 45) rotation = i.o > 0 ? -1 : 1;
        }
    }

    function land() {
        if (i.a < 20 && abs.distance > 1) {
            if (
                (i.h > 0 && i.distance > 0 && i.x > target.x) ||
                (i.h < 0 && i.distance < 0 && i.x < target.x)
            ) {
                translate(target.x);
            }

        } else {
            upright();
            thrust = 0;
            if (i.a < 10) maxV = 5;
            else maxV = i.a / 3;

            if (i.v > maxV) { thrust = 1; }
        }
    }
    
    function speedLimit(s) {
        if (i.h > s) {
            throttle = 1;
            rotation = i.h > 0 ? -1 : 1;
        }
    }

    function throttleLimit(t) {
        if (abs.h > t && samesign(i.h, i.distance)) {
            thrust = 0;
        }
    }

    function upright() {
        if (abs.o < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }

    function maxTilt(t) {
        if (abs.o == t) rotation = 0;
        else if (abs.o >= t) rotation = i.o > 0 ? -1 : 1;
    }

    function samesign(a, b) {
        return ((a > 0 && b > 0) || (a < 0 && b < 0));
    }

    translate(target.x);
    if (abs.distance < 20 && abs.h < 5) land();

    return [thrust, rotation];
}