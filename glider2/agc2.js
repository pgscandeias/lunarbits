function AGC (instruments, terrain) {
    /* Game parameters */
    floorY = 700;
    xConst = 0.005;
    gAccel = 0.1;
    xDrag = 0.9997;
    xDrift = 1;
    maxFallSpeed = 7;
    landerWidth = 5;

    /* Convenience */
    i = instruments;
    i.d = i.h > 0 ? 1 : -1;
    i.x = i.r;
    rotation = 0;
    thrust = 0;


    /* Get terrain, accounting for scrolling */
    function getScrollFactor(x) {
        return Math.ceil(x / terrain.length) - 1
    }
    function getTerrain(x) {
        tx = Math.floor(x);
        scrollFactor = getScrollFactor(x);
        tx = tx - 1 - terrain.length * scrollFactor;
        rawTerrainPoint = terrain[tx];
        if (!rawTerrainPoint) console.log(x);
        return {
            x: tx,
            p: rawTerrainPoint.p,
            h: rawTerrainPoint.h,
            a: floorY - rawTerrainPoint.h
        };
    }

    /* Get terrain altitude */
    function getTerrainAltitude(x) {

        t1 = getTerrain(Math.floor(x));
        t2 = getTerrain(Math.ceil(x));

        // determine slope...
        slope = t2.h - t1.h;

        // ... and calculate altitude
        xGain = x - t1.x;
        hGain = slope / xGain;

        return t1.a + hGain;
    }

    /* Get all available landing spots with score multipliers */
    function getLandingZones() {
        x1 = Math.floor(i.x) - 50;
        x2 = Math.floor(i.x) + 850;
        lzs = [];

        for (var x = x1; x < x2; x++) {
            t = getTerrain(x);
            if (t.p > 1) {
                padLeft = x-1;
                while (getTerrain(x).p == t.p) x++;
                padRight = x-1;
                padX = (padLeft + padRight) / 2 + 2.25 - xDrift * getScrollFactor(x);
                lzRange = getRange(padX);

                lzs.push({
                    l: padLeft,
                    r: padRight,
                    x: padX,
                    w: padRight - padLeft,
                    p: t.p,
                    a: t.a,
                    range: lzRange,
                    score: getlzScore(t, padX, lzRange)
                });
            }
            x++;
        }
        return lzs;
    }

    function getlzScore(t, x, range) {
        cost = Math.abs(x - i.fall);
        
        if (
            (x > i.fall && i.h < 0) ||
            (x < i.fall && i.h > 0)
        ) cost += Math.abs(i.h);
        else cost -= Math.abs(i.h);

        return t.p * 50 * 2 - cost;
    }

    function getBestLz() {
        lzs = getLandingZones();
        lz = { score: -99999 };
        for (var n in lzs) {
            if (lzs[n].score > lz.score) lz = lzs[n];
        }

        return lz;
    }

    /* Landing instrumentation */
    function getGlideProfile(t, tolerance) {
        landDelta = t.x - i.fall;
        if (Math.abs(landDelta) < tolerance) return 'bullseye';
        else if (landDelta < 0) return 'right';
        else if (landDelta > 0) return 'left';
    }

    function getGlideTolerance(t) {
        gt = (t.w - landerWidth) / 2;
        if (gt > 10) gt = 10;
        if (gt < 0.5) gt = 0.5;

        return gt;
    }

    function getRange(x) {
        return Math.sqrt(Math.pow(x - i.x, 2) + Math.pow(getTerrainAltitude(a) - i.abs_a, 2));
    }

    function getFallLocation() {
        x = i.x;
        a = i.abs_a;
        h = i.h;
        v = i.v;
        ta = getTerrainAltitude(x);

        /* Simulate every single frame till the lander drops */
        /* This wouldn't be necessary if drag was constant *ahem* */
        while(a > ta) {
            // Move
            x += h * xConst;
            a -= v * xConst;

            // Fall and drag
            h = h * xDrag;
            v += gAccel;

            // Get ground altitude
            ta = getTerrainAltitude(x);
        }

        return x;
    }

    function getMaxV(t) {
        targetA = i.abs_a - t.a;
        if (i.a < a) a = i.a;
        else a = targetA;

        maxV = a / 4;
        return maxV > maxFallSpeed ? maxV : maxFallSpeed;
    }

    function getTiltLimit() {
        tl = 90;
        if (i.a < 10) tl = 10;

        return tl;
    }


    /* Flight controls */
    function upright() {
        if (Math.abs(i.o) < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }
    function boostUp() {
        upright();
        thrust = 1;
    }

    function boostRight() {
        if (i.o < 0 || Math.abs(i.o) < getTiltLimit()) rotation = 1;
        if (i.o >= 45) thrust = 1;
    }

    function boostLeft() {
        if (i.o > 0 || Math.abs(i.o) < getTiltLimit()) rotation = -1;
        if (i.o <= -45) thrust = 1;
    }

    /* Update instruments */
    i.abs_a = i.a + getTerrainAltitude(i.x);
    i.fall = getFallLocation();


    /* Target selection */
    target = getBestLz();
    targetTolerance = getGlideTolerance(target);
    glideProfile = getGlideProfile(target, targetTolerance);


    /* Approach */
    upright();
    if (glideProfile == 'right') boostLeft();
    else if (glideProfile == 'left') boostRight();

    /* Descent control */
    if (i.v > getMaxV(target)) boostUp();

    /* Landing position */
    if (i.a < 0.75) upright();


    console.log(
        '\n--------------'+
        '\nfall:  '+i.fall+
        '\nrange: '+target.range+
        '\ni.x:   '+i.x+
        '\ntgt.x: '+target.x+' (+- '+targetTolerance+')'
    );

    /* Output */
    return [thrust, rotation];
}