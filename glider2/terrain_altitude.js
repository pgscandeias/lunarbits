function AGC (instruments, terrain) {
    /* Game parameters */
    floorY = 700;
    xConst = 0.005;
    gAccel = 0.1;
    xDrag = 0.9997;

    /* Convenience */
    i = instruments;
    i.d = i.h > 0 ? 1 : -1;
    rotation = 0;
    thrust = 0;


    /* Get terrain, accounting for scrolling */
    function getTerrain(x) {
        tx = Math.floor(x);
        scrollFactor = Math.ceil(x / terrain.length) - 1;
        tx = tx - terrain.length * scrollFactor;
        rawTerrainPoint = terrain[tx];
        if (rawTerrainPoint) {
            return {
                x: tx,
                p: rawTerrainPoint.p,
                h: rawTerrainPoint.h,
                a: floorY - rawTerrainPoint.h
            };
        } else {
            return {
                x: null,
                p: null,
                h: null,
                a: null
            };
        }
    }

    /* Get terrain H */
    function getTerrainH(x) {

        t1 = getTerrain(Math.floor(x));
        t2 = getTerrain(Math.ceil(x));

        slope = t2.h - t1.h;
        xGain = x - t1.x;
        hGain = slope * xGain;
        i.hGain = hGain;

        if (slope == 0) i.slope = 'flat';
        else i.slope = slope > 0 ? 'uphill' : 'downhill';

        h = t1.h + hGain;

        return h;
    }

    function getRange(x) {
        return Math.sqrt(Math.pow(x - i.x, 2) + Math.pow(getTerrainAltitude(a) - i.abs_a, 2));
    }


    /* Flight controls */
    function upright() {
        if (Math.abs(i.o) < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }
    function boostUp() {
        upright();
        thrust = 1;
    }

    function boostRight() {
        rotation = 1;
        if (i.o >= 45) thrust = 1;
    }

    function boostLeft() {
        rotation = -1;
        if (i.o <= -45) thrust = 1;
    }

    /* Update instruments */
    /*i.abs_a = i.a + getTerrainAltitude(i.x);*/
    

    /* Take terrain measurements */
    upright();
    
    function upright() {
        if (Math.abs(i.o) < 10) rotation = 0;
        else rotation = i.o > 0 ? -1 : 1;
    }

    function hover(tolerance) {
        if (i.v < tolerance) thrust = 0;
        if (i.v > tolerance) {
            thrust = 1;
            if (Math.abs(i.o) > 45) rotation = i.o > 0 ? -1 : 1;
        }
    }

    upright();
    hover(0);

    /*
    console.log(
        '\n--------------'+
        '\ni.a:    '+i.a+
        '\ni.abs_a:'+i.abs_a
    );
    */
    abs_a = floorY - getTerrainH(i.x) + i.a;

    if (!window.ship) window.ship = { minA: floorY, maxA: 0 };
    if (abs_a < window.ship.minA) window.ship.minA = abs_a;
    if (abs_a > window.ship.maxA) window.ship.maxA = abs_a;

    console.log(
        '\n---------'+
        '\nmin a: '+window.ship.minA+
        '\nmax a: '+window.ship.maxA
    );


    /* Output */
    return [thrust, rotation];
}